if(isServer) then {
    _heli = ["B_Clouds_Heli_Light_02_unarmed_clouds_01_F", markerPos "v_spawn_0", markerDir "v_spawn_0"] call KyuFunctions_fnc_createVehicle;
    _heli addMagazineTurret ["300Rnd_CMFlare_Chaff_Magazine",[-1]];

    _heli = ["B_Clouds_Heli_Light_02_unarmed_clouds_01_F", markerPos "v_spawn_1", markerDir "v_spawn_1"] call KyuFunctions_fnc_createVehicle;
    _heli addMagazineTurret ["300Rnd_CMFlare_Chaff_Magazine",[-1]];

    _heli = ["B_Clouds_Heli_Light_02_unarmed_clouds_01_F", markerPos "v_spawn_2", markerDir "v_spawn_2"] call KyuFunctions_fnc_createVehicle;
    _heli addMagazineTurret ["300Rnd_CMFlare_Chaff_Magazine",[-1]];

    _heli = ["B_Clouds_Heli_Light_02_unarmed_clouds_01_F", markerPos "v_spawn_3", markerDir "v_spawn_3"] call KyuFunctions_fnc_createVehicle;
    _heli addMagazineTurret ["300Rnd_CMFlare_Chaff_Magazine",[-1]];

    _heli = ["B_Clouds_Heli_Light_02_unarmed_clouds_01_F", markerPos "v_spawn_4", markerDir "v_spawn_4"] call KyuFunctions_fnc_createVehicle;
    _heli addMagazineTurret ["300Rnd_CMFlare_Chaff_Magazine",[-1]];

    _heli = ["B_Clouds_Heli_Light_02_unarmed_clouds_01_F", markerPos "v_spawn_5", markerDir "v_spawn_5"] call KyuFunctions_fnc_createVehicle;
    _heli addMagazineTurret ["300Rnd_CMFlare_Chaff_Magazine",[-1]];

    ["O_Flames_Offroad_01_flames_01_F", getMarkerPos "v_spawn_6", markerDir "v_spawn_6"] call KyuFunctions_fnc_createVehicle;
    ["O_Flames_Offroad_01_flames_01_F", markerPos "v_spawn_7", markerDir "v_spawn_7"] call KyuFunctions_fnc_createVehicle;

    _heli = [
        "B_Clouds_Heli_Light_02_unarmed_clouds_01_F", markerPos "v_spawn_8", markerDir "v_spawn_8", west,
        "\a3\air_f\Heli_Light_02\Data\heli_light_02_ext_indp_co.paa"
    ] call KyuFunctions_fnc_createVehicle;
    _heli addMagazineTurret ["300Rnd_CMFlare_Chaff_Magazine",[-1]];

/*
    _heli addWeaponTurret ["CMFlareLauncher",[-1]];
    _heli addMagazineTurret ["300Rnd_CMFlare_Chaff_Magazine",[-1]];
    _heli addMagazineTurret ["240Rnd_CMFlareMagazine",[-1]];

    _heli addWeaponTurret ["M134_minigun",[-1]];
    _heli addMagazineTurret ["5000Rnd_762x51_Belt",[-1]];

    _heli addWeaponTurret ["missiles_DAGR",[-1]];
    _heli addMagazineTurret ["24Rnd_PG_missiles",[-1]];

    _heli addWeaponTurret ["missiles_DAR",[-1]];
    _heli addMagazineTurret ["24Rnd_missiles",[-1]];

    _heli addWeaponTurret ["GMG_20mm",[-1]];
    _heli addMagazineTurret ["200Rnd_20mm_G_belt",[-1]];

    _heli addWeaponTurret ["gatling_20mm",[-1]];
    _heli addMagazineTurret ["2000Rnd_20mm_shells",[-1]];

    _heli addWeaponTurret ["gatling_30mm",[-1]];
    _heli addMagazineTurret ["250Rnd_30mm_HE_shells",[-1]];
    _heli addMagazineTurret ["250Rnd_30mm_APDS_shells",[-1]];

    _heli addWeaponTurret ["missiles_ASRAAM",[-1]];
    _heli addMagazineTurret ["4Rnd_AAA_missiles",[-1]];
    _heli addMagazineTurret ["4Rnd_AAA_missiles_MI02",[-1]];

    _heli addWeaponTurret ["missiles_SCALPEL",[-1]];
    _heli addMagazineTurret ["8Rnd_LG_scalpel",[-1]];

    _heli addWeaponTurret ["missiles_titan",[-1]];
    _heli addMagazineTurret ["5Rnd_GAT_missiles",[-1]];
    _heli addMagazineTurret ["4Rnd_GAA_missiles",[-1]];
    _heli addMagazineTurret ["4Rnd_Titan_long_missiles",[-1]];

    _heli addWeaponTurret ["rockets_Skyfire",[-1]];
    _heli addMagazineTurret ["38Rnd_80mm_rockets",[-1]];

    _heli addWeaponTurret ["cannon_120mm",[-1]];
    _heli addMagazineTurret ["32Rnd_120mm_APFSDS_shells",[-1]];
    _heli addMagazineTurret ["30Rnd_120mm_HE_shells",[-1]];

    _heli addWeaponTurret ["cannon_125mm",[-1]];
    _heli addMagazineTurret ["24Rnd_125mm_APFSDS",[-1]];
    _heli addMagazineTurret ["12Rnd_125mm_HE",[-1]];
    _heli addMagazineTurret ["12Rnd_125mm_HEAT",[-1]];

    _heli addWeaponTurret ["cannon_105mm",[-1]];
    _heli addMagazineTurret ["40Rnd_105mm_APFSDS",[-1]];
    _heli addMagazineTurret ["20Rnd_105mm_HEAT_MP",[-1]];

    _heli addWeaponTurret ["gatling_25mm",[-1]];
    _heli addMagazineTurret ["1000Rnd_25mm_shells",[-1]];

    _heli addWeaponTurret ["autocannon_35mm",[-1]];
    _heli addMagazineTurret ["680Rnd_35mm_AA_shells",[-1]];

    _heli addWeaponTurret ["missiles_Zephyr",[-1]];
    _heli addMagazineTurret ["4Rnd_GAA_missiles",[-1]];

    _heli addWeaponTurret ["missiles_titan_static",[-1]];
    _heli addMagazineTurret ["1Rnd_GAT_missiles",[-1]];
    _heli addMagazineTurret ["1Rnd_GAA_missiles",[-1]];

    _heli addWeaponTurret ["GBU12BombLauncher",[-1]];
    _heli addMagazineTurret ["2Rnd_GBU12_LGB",[-1]];
    _heli addMagazineTurret ["2Rnd_GBU12_LGB_MI10",[-1]];

    _heli addWeaponTurret ["Mk82BombLauncher",[-1]];
    _heli addMagazineTurret ["2Rnd_Mk82",[-1]];
    _heli addMagazineTurret ["2Rnd_Mk82_MI08",[-1]];

    _heli addWeaponTurret ["rockets_230mm_GAT",[-1]];
    _heli addMagazineTurret ["12Rnd_230mm_rockets",[-1]];
    _heli addWeaponTurret ["LMG_coax",[-1]];
    _heli addMagazineTurret ["2000Rnd_762x51_Belt",[-1]];

    _heli addWeaponTurret ["autocannon_30mm",[-1]];
    _heli addMagazineTurret ["140Rnd_30mm_MP_shells",[-1]];
    _heli addMagazineTurret ["60Rnd_30mm_APFSDS_shells",[-1]];

    _heli addWeaponTurret ["cannon_120mm_long",[-1]];
    _heli addMagazineTurret ["28Rnd_120mm_APFSDS_shells",[-1]];
    _heli addMagazineTurret ["14Rnd_120mm_HE_shells",[-1]];

    _heli addWeaponTurret ["Twin_Cannon_20mm",[-1]];
    _heli addMagazineTurret ["2000Rnd_20mm_shells",[-1]];

    _heli addWeaponTurret ["Gatling_30mm_Plane_CAS_01_F",[-1]];
    _heli addMagazineTurret ["1000Rnd_Gatling_30mm_Plane_CAS_01_F",[-1]];

    _heli addWeaponTurret ["Missile_AA_04_Plane_CAS_01_F",[-1]];
    _heli addMagazineTurret ["2Rnd_Missile_AA_04_F",[-1]];

    _heli addWeaponTurret ["Missile_AGM_02_Plane_CAS_01_F",[-1]];
    _heli addMagazineTurret ["6Rnd_Missile_AGM_02_F",[-1]];

    _heli addWeaponTurret ["Rocket_04_HE_Plane_CAS_01_F",[-1]];
    _heli addMagazineTurret ["7Rnd_Rocket_04_HE_F",[-1]];

    _heli addWeaponTurret ["Rocket_04_AP_Plane_CAS_01_F",[-1]];
    _heli addMagazineTurret ["7Rnd_Rocket_04_AP_F",[-1]];

    _heli addWeaponTurret ["Bomb_04_Plane_CAS_01_F",[-1]];
    _heli addMagazineTurret ["4Rnd_Bomb_04_F",[-1]];

    _heli addWeaponTurret ["Cannon_30mm_Plane_CAS_02_F",[-1]];
    _heli addMagazineTurret ["500Rnd_Cannon_30mm_Plane_CAS_02_F",[-1]];

    _heli addWeaponTurret ["Missile_AA_03_Plane_CAS_02_F",[-1]];
    _heli addMagazineTurret ["2Rnd_Missile_AA_03_F",[-1]];

    _heli addWeaponTurret ["Missile_AGM_01_Plane_CAS_02_F",[-1]];
    _heli addMagazineTurret ["4Rnd_Missile_AGM_01_F",[-1]];

    _heli addWeaponTurret ["Rocket_03_HE_Plane_CAS_02_F",[-1]];
    _heli addMagazineTurret ["20Rnd_Rocket_03_HE_F",[-1]];

    _heli addWeaponTurret ["Rocket_03_AP_Plane_CAS_02_F",[-1]];
    _heli addMagazineTurret ["20Rnd_Rocket_03_AP_F",[-1]];

    _heli addWeaponTurret ["Bomb_03_Plane_CAS_02_F",[-1]];
    _heli addMagazineTurret ["2Rnd_Bomb_03_F",[-1]];

    _heli addWeaponTurret ["HMG_127_MBT",[-1]];
    _heli addMagazineTurret ["500Rnd_127x99_mag",[-1]];

    _heli addWeaponTurret ["HMG_127_LSV_01",[-1]];
    _heli addMagazineTurret ["500Rnd_127x99_mag",[-1]];

    _heli addWeaponTurret ["MMG_02_vehicle",[-1]];
    _heli addMagazineTurret ["130Rnd_338_Mag",[-1]];

    _heli addWeaponTurret ["gatling_20mm_VTOL_01",[-1]];
    _heli addMagazineTurret ["4000Rnd_20mm_Tracer_Red_shells",[-1]];

    _heli addWeaponTurret ["autocannon_40mm_VTOL_01",[-1]];
    _heli addMagazineTurret ["240Rnd_40mm_GPR_Tracer_Red_shells",[-1]];
    _heli addMagazineTurret ["160Rnd_40mm_APFSDS_Tracer_Red_shells",[-1]];

    _heli addWeaponTurret ["cannon_105mm_VTOL_01",[-1]];
    _heli addMagazineTurret ["40Rnd_105mm_APFSDS",[-1]];
    _heli addMagazineTurret ["100Rnd_105mm_HEAT_MP",[-1]];

    _heli addWeaponTurret ["gatling_30mm_VTOL_02",[-1]];
    _heli addMagazineTurret ["250Rnd_30mm_HE_shells",[-1]];
    _heli addMagazineTurret ["250Rnd_30mm_APDS_shells",[-1]];

    _heli addWeaponTurret ["missiles_Jian",[-1]];
    _heli addMagazineTurret ["4Rnd_LG_Jian",[-1]];
*/

    ["O_Flames_Offroad_01_flames_01_F", markerPos "v_spawn_9", markerDir "v_spawn_9"] call KyuFunctions_fnc_createVehicle;

    _heli = ["B_Clouds_Heli_Light_02_unarmed_clouds_01_F", markerPos "v_spawn_10", markerDir "v_spawn_10"] call KyuFunctions_fnc_createVehicle;
    _heli addMagazineTurret ["300Rnd_CMFlare_Chaff_Magazine",[-1]];

    ["O_Flames_Offroad_01_flames_01_F", markerPos "v_spawn_11", markerDir "v_spawn_11"] call KyuFunctions_fnc_createVehicle;
    ["O_Flames_Offroad_01_flames_01_F", markerPos "v_spawn_12", markerDir "v_spawn_12"] call KyuFunctions_fnc_createVehicle;
    ["O_Flames_Offroad_01_flames_01_F", markerPos "v_spawn_13", markerDir "v_spawn_13"] call KyuFunctions_fnc_createVehicle;
    ["O_Flames_Offroad_01_flames_01_F", markerPos "v_spawn_14", markerDir "v_spawn_14"] call KyuFunctions_fnc_createVehicle;
    ["O_Flames_Offroad_01_flames_01_F", markerPos "v_spawn_15", markerDir "v_spawn_15"] call KyuFunctions_fnc_createVehicle;
    ["O_Flames_Offroad_01_flames_01_F", markerPos "v_spawn_16", markerDir "v_spawn_16"] call KyuFunctions_fnc_createVehicle;
};