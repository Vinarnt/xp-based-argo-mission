#include "scripts\SHK_Fastrope.sqf"
#include "initKeyboard.sqf";
#include "scripts\radial_local.sqf";
#include "scripts\no_dlc.sqf";
#include "scripts\kill_ticker.sqf";
#include "scripts\crew_hud.sqf";
#include "scripts\leveling\leveling_hud.sqf";
#include "scripts\take_filter.sqf";

[] execVM "scripts\QS_icons.sqf";

if(!isDev) then {
    [] execVM "scripts\cutscenes\intro.sqf";
} else {
    forceRespawn player;
};

player addEventHandler ["InventoryOpened",{
    _this spawn {
        waitUntil {
            not isNull (findDisplay 602);
        };
        INVCLOSED = (findDisplay 602) displayAddEventHandler ["keydown", {
            _display = _this select 0;
            if ((_this select 1) == 23) then {
                (_display) displayRemoveEventHandler ["keydown", INVCLOSED];
                (_display) closeDisplay 1;
            };
        }];
    };
}];

[ missionNamespace, "arsenalClosed", {
    [player, [missionNamespace, "BIS_inv"]] call KyuFunctions_fnc_saveInventory;
}] call BIS_fnc_addScriptedEventHandler;

[ missionNamespace, "arsenalLoadoutLoaded", {
    params ["_unit", "_loadoutName", "_loadoutInventory"];

    private _fnc_check = {
        params ["_unit", "_array"];

        private _checkSuccess = true;
        {
            if(typeName _x == "ARRAY") then {
                if(!([_unit, _x] call _fnc_check)) then { _checkSuccess = false };
            } else {
                if(!([_unit, _x] call KyuFunctions_fnc_checkItemPermission)) then { _checkSuccess = false };
            };
        } forEach _array;

        // return
        _checkSuccess;
    };

    if(!([_unit, _loadoutInventory] call _fnc_check)) then {
        systemChat "Some items has been removed from the loadout because you didn't unlock them yet.";
    };
}] call BIS_fnc_addScriptedEventHandler;

diag_log "Initialized player local";