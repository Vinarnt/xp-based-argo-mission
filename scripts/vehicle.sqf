if (!hasInterface) exitWith {};

seat2gui_keep_open=false;
seat2gui_open_always=false;
seat2gui_display=[nil];

better_select_seat={
    Veh=_this select 0;
    current_player= _this select 1;
    moveOut current_player;
    _names=selectionNames Veh;
    _bx_min=100;     
    _bx_max=-100;     
    _by_min=100;     
    _by_max=-100;
    _filter=[];     
    {     
        if ((_x find "\cargo" >=0) || (_x find "\pilot." >=0) || (_x find "\driver." >=0) || (_x find "\gunner." >=0) || (_x find "\commander." >=0) ) then     
        {
            if !((typeOf Veh) isEqualTo "O_Flames_LSV_01_unarmed_flames_01_F" && (_x find "\gunner." >=0)) then
            {
                _filter append [_x];
                _pos=Veh selectionPosition _x;
                _px=_pos select 1;
                _py=_pos select 0;
                if (_px < _bx_min) then { _bx_min=_px;};
                if (_py < _by_min) then { _by_min=_py;};
                if (_px > _bx_max) then { _bx_max=_px;};
                if (_py > _by_max) then { _by_max=_py;};
            };
        };
    }forEach _names;

    _display = findDisplay 46 createDisplay "RscDisplayEmpty";
    seat2gui_display=[_display];
    _frame = _display ctrlCreate ["IGUIBack", 645];
    _frame ctrlSetForegroundColor [0.25, 0.25, 0.25, 0.05];
    _frame ctrlSetPosition [0.09,0.09,0.76,0.69];
    _frame ctrlCommit 0;
   
    _edit_ck_txt = _display ctrlCreate ["RscText", 645];
    _edit_ck_txt ctrlSetPosition [0.09,0.68,0.17,0.05];
    _edit_ck_txt ctrlSetText "Keep menu open";
    _edit_ck_txt ctrlCommit 0;
   
    _edit_ck = _display ctrlCreate ["RscCheckBox", 645];
    _edit_ck ctrlSetPosition [0.3,0.68,0.05,0.05];
    _edit_ck cbSetChecked  seat2gui_keep_open;
    _edit_ck ctrlAddEventHandler ["CheckedChanged", "seat2gui_keep_open = ((_this select 1) == 1);"];
    _edit_ck ctrlCommit 0;
   
    _edit_ck_txt = _display ctrlCreate ["RscText", 645];
    _edit_ck_txt ctrlSetPosition [0.09,0.72,0.17,0.05]; 
    _edit_ck_txt ctrlSetText "Always show menu";
    _edit_ck_txt ctrlCommit 0;
    
    _edit_ck = _display ctrlCreate ["RscCheckBox", 645];
    _edit_ck ctrlSetPosition [0.3,0.72,0.05,0.05];
    _edit_ck cbSetChecked  seat2gui_open_always;
    _edit_ck ctrlAddEventHandler ["CheckedChanged", "seat2gui_open_always = ((_this select 1) == 1);"];
    _edit_ck ctrlCommit 0;

    _towButton = null;
    _slingButton = null;

    updateTowButton={
        params ["_button", "_isTowed"];

        _button ctrlSetText (["Tow", "Untow"] select _isTowed);
    };

     updateSlingButton={
        params ["_button", "_nearestHeli", "_isTowed", "_canSlingLoad", "_isSlingLoaded", "_isSlingLoading"];

        _button ctrlShow (!_isTowed && (not isNull _nearestHeli) && _canSlingLoad && (!_isSlingLoading || _isSlingLoaded));
        _button ctrlSetText (["Sling Load", "Cut Ropes"] select _isSlingLoaded);
     };

     createTowButton={
        params ["_currentCar", "_nearestCar"];
        private ["_towButton"];

        _towButton = _display ctrlCreate ["RscButton", 646];
        _towButton ctrlSetPosition [0.5, 0.72, 0.15, 0.05];
        _towButton setVariable ["targetCar", _nearestCar];
        _towButton setVariable ["currentCar", _currentCar];
        [_towButton, _currentCar call KyuFunctions_fnc_isTowed] call updateTowButton;

        _towButton ctrlAddEventHandler ["ButtonClick", {
            params ["_button"];

            _targetCar = _button getVariable "targetCar";
            _currentCar = _button getVariable "currentCar";
            _slingButton = _button getVariable "slingButton";

            _isTowed = _currentCar call KyuFunctions_fnc_isTowed;
            _needToTow = !_isTowed;
            _isTowing = _currentCar call KyuFunctions_fnc_isTowing;
            _isTargetTowing = _targetCar call KyuFunctions_fnc_isTowing;
            _isTargetTowed = _targetCar call KyuFunctions_fnc_isTowed;
            _canBeTowed = !_isTowing && !_isTargetTowed && !_isTargetTowing;
            _isSlingLoaded = _currentCar call isSlingLoaded;
            if(_needToTow) then {
                if(_canBeTowed) then {
                    [_currentCar, _targetCar] call KyuFunctions_fnc_tow;
                    _isTowed = true;
                    _slingButton ctrlShow false;
                    systemChat "Towed car successfully";
                } else {
                    systemChat "Unable to tow";
                }
            } else {
                [_currentCar] call KyuFunctions_fnc_untow;
                _isTowed = false;
                _slingButton ctrlShow true;
                systemChat "Untowed car successfully";
            };
            [_button, _isTowed] call updateTowButton;
        }];
        _towButton ctrlCommit 0;

        // return
        _towButton;
     };

     createSlingLoadButton={
        params ["_currentCar", "_nearestHeli"];
        private ["_slingButton"];

        _slingButton = _display ctrlCreate ["RscButton", 647];
        _slingButton ctrlSetPosition [0.68, 0.72, 0.15, 0.05];
        _slingButton setVariable ["targetHeli", _nearestHeli];
        _slingButton setVariable ["currentCar", _currentCar];
        _slingButton setVariable ["towButton", _towButton];
        _towButton setVariable ["slingButton", _slingButton];
        _slingButton ctrlAddEventHandler ["ButtonClick", {
            params ["_button"];

            _targetHeli = _button getVariable "targetHeli";
            _currentCar = _button getVariable "currentCar";
            _towButton = _button getVariable "towButton";
            _isTowed = _currentCar call KyuFunctions_fnc_isTowed;
            _isSlingLoaded = _currentCar call KyuFunctions_fnc_isSlingLoaded;
            _needToSlingLoad = !_isSlingLoaded;
            _isHeliSlingLoading = [_targetHeli] call KyuFunctions_fnc_isSlingLoading;

            // If the car need to be sling loaded
            if(_needToSlingLoad) then {
                if(!_isTowed && !_isHeliSlingLoading) then {
                    _isSlingLoaded = ([_currentCar, _targetHeli] call KyuFunctions_fnc_slingLoad);
                    if(_isSlingLoaded) then {
                        _towButton ctrlShow false;
                        systemChat "Car was sling loaded sucessfully";
                     };
                } else {
                    systemChat "Unable to sling load";
                };
            } else { // If the heli is already sling loading we can unsling
                [_targetHeli] call KyuFunctions_fnc_slingUnload;
                _isSlingLoaded = false;
                _towButton ctrlShow true;
                systemChat "Cut the ropes sucessfully";
            };

            [_button, _targetHeli, _isTowed, true, _isSlingLoaded, _isSlingLoading] call updateSlingButton
        }];
        _slingButton ctrlCommit 0;

        // return
        _slingButton;
     };

     if(Veh isKindOf "LandVehicle") then {
        // Tow button
        _nearestCar = Veh call KyuFunctions_fnc_getNearestTowableCar;
        _isTowed =  Veh call KyuFunctions_fnc_isTowed;
        _hasTowableCar = not isNil "_nearestCar";
        _show = (!_hasTowableCar && _isTowed);
        if(_hasTowableCar) then {
            _isTowing =  Veh call KyuFunctions_fnc_isTowing;
            _isTargetTowing = _nearestCar call KyuFunctions_fnc_isTowing;
            _isTargetTowed = _nearestCar call KyuFunctions_fnc_isTowed;
            _show = _isTowed || (!_isTowing && !_isTargetTowed && !_isTargetTowing);
        };

        if(_show) then {
            _towButton = [Veh, _nearestCar] call createTowButton;
            _towButton ctrlShow !([Veh] call KyuFunctions_fnc_isSlingLoaded);
        };

        // Sling load button
        _nearestHeli = nearestObject [Veh, "Helicopter"];
        _canSlingLoad = _nearestHeli canSlingLoad Veh;
        _isSlingLoaded = [Veh] call KyuFunctions_fnc_isSlingLoaded;
        _isSlingLoading = [_nearestHeli] call KyuFunctions_fnc_isSlingLoading;
        _slingButton = [Veh, _nearestHeli] call createSlingLoadButton;

        [_slingButton, _nearestHeli, _isTowed, _canSlingLoad, _isSlingLoaded, _isSlingLoading] call updateSlingButton
     };
    
    { // forEach
        _pos=Veh selectionPosition _x;
        _px=(((_pos select 1)-_bx_min)/(_bx_max-_bx_min));
        _py=(((_pos select 0)-_by_min)/(_by_max-_by_min));
        _txt=toArray(_x);
        reverse _txt;
        _idx=toString(_txt) find "\";
        _crew_var=_x select [(count _x) - _idx,count _x];
        _px=floor(_px * 30.0)/30.0;
        _py=floor(_py * 30.0)/30.0;
        _px=0.10+_px * 0.6;
        _py=0.10+_py * 0.45;
        _edit_ck_txt = _display ctrlCreate ["RscButtonMenu", 645];
        _edit_ck_txt ctrlSetPosition [_px,_py,0.13,0.13];
        _edit_ck_txt ctrlSetBackgroundColor [0.25, 0.25, 0.25, 0.5];
        _edit_ck_txt ctrlSetFont "RobotoCondensedLight";
        _edit_ck_txt ctrlSetText str(_crew_var);
        _code={hint str(Veh);};

        if ((_crew_var find "pilot" >=0) || (_crew_var find "driver" >=0)) then
        {
            _entity=driver Veh;
            if !(isNull(_entity)) then {
                _edit_ck_txt ctrlSetText name _entity;
                if (alive _entity) then {
                    _edit_ck_txt ctrlSetBackgroundColor [0.25, 0.45, 0.25, 0.5];
                } else {
                    _edit_ck_txt ctrlSetBackgroundColor [0.45, 0.25, 0.25, 0.5];
                    _entity setPos (getPos _entity);
                };
            } else {
                _edit_ck_txt ctrlSetBackgroundColor [0.25, 0.25, 0.45, 0.5];
            };
            _code={moveOut current_player;current_player moveInDriver Veh; if (!seat2gui_keep_open) then {(seat2gui_display select 0) closeDisplay 1;};};
        } ;
        if ((_crew_var find "commander" >=0)) then
        {
            _entity=commander Veh;
            if !(isNull(_entity)) then {
                _edit_ck_txt ctrlSetText name _entity;
                if (alive _entity) then {
                    _edit_ck_txt ctrlSetBackgroundColor [0.25, 0.45, 0.25, 0.5];
                } else {
                    _edit_ck_txt ctrlSetBackgroundColor [0.45, 0.25, 0.25, 0.5];
                    _entity setPos (getPos _entity);
                };
            } else {
                _edit_ck_txt ctrlSetBackgroundColor [0.25, 0.25, 0.35, 0.5];
            };
            _code={moveOut current_player;current_player moveInCommander Veh;if (!seat2gui_keep_open) then {(seat2gui_display select 0) closeDisplay 1;};};
        } ;
        if (_crew_var find "cargo" >=0) then
        {
            _entity=objNull;
            _shift=1;

            if ((typeOf Veh) isEqualTo "O_Flames_LSV_01_unarmed_flames_01_F") then {_shift=0;};
            _path= parseNumber(_crew_var select [(_crew_var find ".")+1, count _crew_var]);
            _code="moveout current_player; current_player moveInCargo [Veh," + str(_path-_shift)+ "];if (!seat2gui_keep_open) then {(seat2gui_display select 0) closeDisplay 1;};";

            {
                if ((_x select 2) == _path-_shift) then {_entity= _x select 0;};
            } forEach fullCrew [Veh,"",false];

            if !(isNull(_entity)) then {
                _edit_ck_txt ctrlSetText name _entity;
                if (alive _entity) then {
                    _edit_ck_txt ctrlSetBackgroundColor [0.25, 0.45, 0.25, 0.5];
                } else {
                    _edit_ck_txt ctrlSetBackgroundColor [0.45, 0.25, 0.25, 0.5];
                    _entity setPos (getPos _entity);
                };
            } else {
                _edit_ck_txt ctrlSetBackgroundColor [0.25, 0.25, 0.35, 0.5];
            };
        };
        if (_crew_var find "gunner" >=0) then
        {
            _entity=objNull;

            _path= parseNumber(_crew_var select [(_crew_var find ".")+1, count _crew_var]);
            _code="moveout current_player; current_player moveInTurret [Veh,[" + str(_path-1)+ "]];if (!seat2gui_keep_open) then {(seat2gui_display select 0) closeDisplay 1;};";
            {
                if ((_x select 3) select 0 == _path-1) then {_entity= _x select 0;};
            } forEach fullCrew [Veh,"",false];

            if !(isNull(_entity)) then {
                _edit_ck_txt ctrlSetText name _entity;
                if (alive _entity) then {
                    _edit_ck_txt ctrlSetBackgroundColor [0.25, 0.45, 0.25, 0.5];
                } else {
                    _edit_ck_txt ctrlSetBackgroundColor [0.45, 0.25, 0.25, 0.5];
                    _entity setPos (getPos _entity);
                };
            } else {
                _edit_ck_txt ctrlSetBackgroundColor [0.25, 0.25, 0.35, 0.5];
            };
        };
        _edit_ck_txt ctrlAddEventHandler ["ButtonClick", _code];
        _edit_ck_txt ctrlCommit 0;
    } forEach _filter;
     
};

inGameUISetEventHandler ["Action", "
        if ((_this select 3) in [""GetInDriver"",""GetInPilot""]) then {
            if (seat2gui_open_always) then
            {
                [_this select 0, _this select 1] call better_select_seat;
            } else {
                (_this select 1) moveInDriver (_this select 0);
            };
            true;      
        } else {
            if ((_this select 3) in [""GetInCargo"",""GetInTurret""]) then {
                [_this select 0, _this select 1] call better_select_seat;
                true;
            } else {
                false;   
            };
        };
    "];