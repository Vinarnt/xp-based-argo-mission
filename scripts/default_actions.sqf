get_default_action={
    _actions=[];
    if (_this isKindOf "Ship" && locked _this < 2) then {
        _actions =[
            ["> Move in <", {
                params ["_action", "_who", "_what", "_args"];

                [_what,_who] spawn boatcrew;
            }],
			["> Drive <", {[_what,_who] spawn boatDriving;}]
        ];
    };
    if ((_this isKindOf "LandVehicle" || _this isKindOf "Air") && locked _this < 2) then {
        {
            _x params ["_unit", "_role", "_cargoIndex", "_turretPath", "_personTurret"];
            _title="default";
            switch (toLower(_role)) do
            {
                case "driver": {
                    if (isNull _unit) then {
                        _title= "> Move in driver <";
                    } else {
                        _title= "< " + name _unit + " >";
                    };
                    if ([configFile >> "CfgVehicles" >> typeOf _this , "hasDriver",2] call BIS_fnc_returnConfigEntry > 0) then
                    {
                        _actions append [
                            [_title, {
                                params ["_action", "_who", "_what", "_args"];
                                _who moveInDriver _what;
                            }]
                        ];
                    };
                };
                case "commander": {
                    if (isNull _unit) then {
                        _title= "> Move in commander <";
                    } else {
                        _title= "< " + name _unit + " >";
                    };
                    if ([configFile >> "CfgVehicles" >> typeOf _this , "hasCommander",2] call BIS_fnc_returnConfigEntry > 0) then
                    {
                        _actions append [
                            [_title, {
                                params ["_action", "_who", "_what", "_args"];

                                _who moveInCommander _what;
                            }]
                        ];
                    };
                };
                case "gunner": {
                    if (isNull _unit) then {
                        _title= "> Move in gunner <";
                    } else {
                        _title= "< " + name _unit + " >";
                    };
                    if ([configFile >> "CfgVehicles" >> typeOf _this , "hasGunner",2] call BIS_fnc_returnConfigEntry > 0) then
                    {
                        _actions append [
                            [_title, {
                                params ["_action", "_who", "_what", "_args"];

                                _who moveInGunner _what;
                            }]
                        ];
                    };
                };
                case "turret": {
                    if (isNull _unit) then {
                        _title= "> Move in turret <";
                    } else {
                        _title= "< " + name _unit + " >";
                    };
                    _actions append [
                        [_title, {
                            params ["_action", "_who", "_what", "_args"];

                            _who moveInTurret [_what, _args];
                        }, _turretPath]
                    ];
                };
                case "cargo": {
                    if (isNull _unit) then {
                        _title= "> Move in cargo <";
                    } else {
                        _title= "< " + name _unit + " >";
                    };
                    _actions append [
                        [_title, {
                            params ["_action", "_who", "_what", "_args"];

                            _who moveInCargo [_what, _args];
                        }, _cargoIndex]
                    ];
                };
            };
        } forEach fullCrew [_this, "", true];
    };
    
    _actions;
};