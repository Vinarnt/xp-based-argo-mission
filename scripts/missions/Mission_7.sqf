if(isServer) then {
    AO_7_obj = "O_UAV_02_F" createVehicle (markerPos "AO_7_obj");
    AO_7_obj setDir markerDir "AO_7_obj";
    AO_7_obj allowDamage false;
    AO_7_obj lock 2;

    AO_7_group = "AO_7_group" call KyuFunctions_fnc_loadGroup;
    AO_7_group deleteGroupWhenEmpty true;
    doStop units AO_7_group;
    { _x enableSimulation true; } forEach units AO_7_group;

    [
        AO_7_obj,
        "Place charge",
        "\a3\ui_f\data\IGUI\Cfg\simpleTasks\types\destroy_ca.paa",
        "\a3\ui_f\data\IGUI\Cfg\simpleTasks\types\destroy_ca.paa",
        "_this distance _target < 3",
        "_caller distance _target < 3",
        {},
        {},
        { [] remoteExec ["AO_7_exec", 0] },
        {},
        [],
        6,
        0,
        false,
        false
    ] remoteExec ["BIS_fnc_holdActionAdd", 0, AO_7_obj];

    [true, "AO_7_task", ["", "Destroy the UAV", ""], getPosATL AO_7_obj, "AUTOASSIGNED", 1, true, "destroy", true] call BIS_fnc_taskCreate;

    AO_7_exec = {
        AO_7_obj say3D "assemble_target";
        [AO_7_obj] remoteExec ["removeAllActions", 0, AO_7_obj];
        sleep 10;
        AO_7_obj setDamage 1;
        sleep 1;
        [position AO_7_obj] spawn KyuFunctions_fnc_spawnFireworks;
        ["AO_7_task", "SUCCEEDED"] call BIS_fnc_taskSetState;
        _teleport = ["AO_7_teleport", "base"] call KyuFunctions_fnc_createTeleportBoard;
        sleep 1;
        [] execVM "scripts\missions\Mission_8.sqf";
        sleep 30;
        [ "AO_7_task" ] call BIS_fnc_deleteTask;
        sleep 600;
        {
            deleteVehicle _x;
            sleep 0.1;
        } forEach units AO_7_group;
        deleteVehicle _teleport;
    };
};