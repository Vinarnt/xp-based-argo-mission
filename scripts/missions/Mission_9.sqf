if(isServer) then {
    private ["_group", "_position", "_buildings"];

    _position = getMarkerPos "AO_9";

    // Close all doors
    _buildings = nearestObjects [_position, ["House", "Building"], 200];
    {
        [_x, 0] call KyuFunctions_fnc_setBuildingDoorsState;
        sleep 0.01;
    } forEach _buildings;

    _group = "AO_9_group" call KyuFunctions_fnc_loadGroup;
    _group deleteGroupWhenEmpty true;
    doStop units _group;
    { _x enableSimulation true; } forEach units _group;

    "AO_9" setMarkerAlpha 1;
    _task = [true, "AO_9_task", ["", "Clear the city", ""], _position, "AUTOASSIGNED", 1, true, "attack", true] call BIS_fnc_taskCreate;

    waitUntil {
        sleep 5;
        {alive _x} count (units _group) < 1;
    };

    // Mission success
    "AO_9" setMarkerAlpha 0;
    [_position] spawn KyuFunctions_fnc_spawnFireworks;
    ["AO_9_task", "SUCCEEDED"] call BIS_fnc_taskSetState;
    _teleport = ["AO_9_teleport", "base"] call KyuFunctions_fnc_createTeleportBoard;
    sleep 1;
    [] execVM "scripts\missions\Mission_10.sqf";
    sleep 30;
    ["AO_9_task", true] call BIS_fnc_deleteTask;
    sleep 600;
    deleteVehicle _teleport;
};