if(isServer) then {
    private ["_group", "_position"];

    _position = getMarkerPos "AO_2";

    _buildings = nearestObjects [_position, ["House", "Building"], 200];
    {
       [_x, 0] call KyuFunctions_fnc_setBuildingDoorsState;
       sleep 0.01;
    } forEach _buildings;

    _group = "AO_2_group" call KyuFunctions_fnc_loadGroup;
    _group deleteGroupWhenEmpty true;
    doStop units _group;
    { _x enableSimulation true; } forEach units _group;

    "AO_2" setMarkerAlpha 1;
    _task = [true, "AO_2_task", ["", "Clear the city", ""], _position, "AUTOASSIGNED", 1, true, "attack", true] call BIS_fnc_taskCreate;

    waitUntil {
        sleep 5;
        { alive _x } count (units _group) <= 20
    };

     _reinforcementGroup = "AO_2_group_1" call KyuFunctions_fnc_loadGroup;
     _reinforcementGroup deleteGroupWhenEmpty true;

    waitUntil {
        sleep 5;
        { alive _x } count (units _group) < 1;
    };

    "AO_2" setMarkerAlpha 0;
    [_position] spawn KyuFunctions_fnc_spawnFireworks;
    ["AO_2_task", "SUCCEEDED"] call BIS_fnc_taskSetState;
    _teleport = ["AO_2_teleport", "base"] call KyuFunctions_fnc_createTeleportBoard;
    sleep 1;
    [] execVM "scripts\missions\Mission_3.sqf";
    sleep 30;
    ["AO_2_task", true] call BIS_fnc_deleteTask;
    sleep 600;
    deleteVehicle _teleport;
};