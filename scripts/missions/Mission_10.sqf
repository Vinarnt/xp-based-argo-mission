if(isServer) then {
    private ["_group", "_position", "_buildings"];

    _position = getMarkerPos "AO_10";

    private _fnc_loadGroup = {
        params ["_groupName"];

        _group = _groupName call KyuFunctions_fnc_loadGroup;
        _group deleteGroupWhenEmpty true;
        doStop units _group;
        { _x enableSimulation true; } forEach units _group;

        // return
        _group;
    };

    private _group1 = "AO_10_group_1" call _fnc_loadGroup;
    "AO_10" setMarkerAlpha 1;
    _task = [true, "AO_10_task", ["", "Clear the city", ""], _position, "AUTOASSIGNED", 1, true, "attack", true] call BIS_fnc_taskCreate;

    waitUntil {
        sleep 5;
        {alive _x} count (units _group1) < 10;
    };

    private _group2 = "AO_10_group_2" call _fnc_loadGroup;
    waitUntil {
        sleep 5;
        {alive _x} count (units _group2) < 10;
    };

    private _group3 = "AO_10_group_3" call _fnc_loadGroup;

    waitUntil {
        sleep 5;
        {alive _x} count (units _group1) < 1
        && {{alive _x} count (units _group2) < 1}
        && {{alive _x} count (units _group3) < 1};
    };

    // Mission success
    "AO_10" setMarkerAlpha 0;
    [_position] spawn KyuFunctions_fnc_spawnFireworks;
    ["AO_10_task", "SUCCEEDED"] call BIS_fnc_taskSetState;
    _teleport = ["AO_10_teleport", "base"] call KyuFunctions_fnc_createTeleportBoard;
    sleep 1;
    [] execVM "scripts\missions\Mission_1.sqf";
    sleep 30;
    ["AO_10_task", true] call BIS_fnc_deleteTask;
    sleep 600;
    deleteVehicle _teleport;
};