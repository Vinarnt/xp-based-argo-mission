if(isServer) then {
    private ["_group"];

    AO_4_holdActionId_1 = -1;
    AO_4_holdActionId_2 = -1;
    AO_4_holdActionId_3 = -1;

    AO_4_fn_loadGroup = {
        params ["_groupName"];

        _group = _groupName call KyuFunctions_fnc_loadGroup;
        _group deleteGroupWhenEmpty true;
        doStop units _group;
        { _x enableSimulation true; } forEach units _group;
        [_group] spawn {
            params ["_group"];

            waitUntil {
                sleep 5;
                !("AO_4_1_task" call BIS_fnc_taskExists)
                && !("AO_4_2_task" call BIS_fnc_taskExists)
                && !("AO_4_3_task" call BIS_fnc_taskExists);
            };
            sleep 600;
            {
                deleteVehicle _x;
            } forEach units _group;
        };
    };

    // Close all doors
    {
        _buildings = nearestObjects [position _x, ["House", "Building"], 50];
        {
            [_x, 0] call KyuFunctions_fnc_setBuildingDoorsState;
            sleep 0.01;
        } forEach _buildings;
    } forEach [AO_4_laptop_1, AO_4_laptop_2, AO_4_laptop_3];

    "AO_4_group_1" call AO_4_fn_loadGroup;
    AO_4_holdActionId_1 = parseNumber ([
        AO_4_laptop_1,
        "Search intels",
        "\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_search_ca.paa",
        "\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_search_ca.paa",
        "_this distance _target < 3",
        "_caller distance _target < 3",
        {},
        {},
        { [] remoteExec ["AO_4_1_exec", 0] },
        {},
        [],
        6,
        0,
        false,
        false
    ] remoteExec ["BIS_fnc_holdActionAdd", 0, AO_4_laptop_1]);

    [true, "AO_4_1_task", ["", "Search for intels", ""], getPosATL AO_4_laptop_1, "AUTOASSIGNED", 1, true, "search", true] call BIS_fnc_taskCreate;

    AO_4_1_exec = {
        [AO_4_laptop_1, AO_4_holdActionId_1] remoteExec ["BIS_fnc_holdActionRemove", 0, AO_4_laptop_1];
        ["AO_4_1_task", "SUCCEEDED"] call BIS_fnc_taskSetState;

        "AO_4_group_2" call AO_4_fn_loadGroup;
        AO_4_holdActionId_2 = parseNumber ([
            AO_4_laptop_2,
            "Search intels",
            "\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_hack_ca.paa",
            "\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_hack_ca.paa",
            "_this distance _target < 3",
            "_caller distance _target < 3",
            {},
            {},
            { remoteExec ["AO_4_2_exec", 0] },
            {},
            [],
            6,
            0,
            false,
            false
        ] remoteExec ["BIS_fnc_holdActionAdd", 0, AO_4_laptop_2]);
        [true, "AO_4_2_task", ["", "Upload virus", ""], getPosATL AO_4_laptop_2, "AUTOASSIGNED", 2, true, "upload", true] call BIS_fnc_taskCreate;
    };

    AO_4_2_exec = {
        [AO_4_laptop_2, AO_4_holdActionId_2] remoteExec ["BIS_fnc_holdActionRemove", 0, AO_4_laptop_2];
        ["AO_4_2_task", "SUCCEEDED"] call BIS_fnc_taskSetState;

        "AO_4_group_3" call AO_4_fn_loadGroup;
        AO_4_holdActionId_3 = parseNumber ([
            AO_4_laptop_3,
            "Search intels",
            "\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_hack_ca.paa",
            "\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_hack_ca.paa",
            "_this distance _target < 3",
            "_caller distance _target < 3",
            {},
            {},
            { [] remoteExec ["AO_4_3_exec", 0] },
            {},
            [],
            6,
            0,
            false,
            false
        ] remoteExec ["BIS_fnc_holdActionAdd", 0, AO_4_laptop_3]);
        [true, "AO_4_3_task", ["", "Download intels", ""], getPosATL AO_4_laptop_3, "AUTOASSIGNED", 3, true, "download", true] call BIS_fnc_taskCreate;
    };

    AO_4_3_exec = {
        [AO_4_laptop_3, AO_4_holdActionId_3] remoteExec ["BIS_fnc_holdActionRemove", 0, AO_4_laptop_3];
        [position AO_4_laptop_3] spawn KyuFunctions_fnc_spawnFireworks;
        ["AO_4_3_task", "SUCCEEDED"] call BIS_fnc_taskSetState;
        _teleport = ["AO_4_teleport", "base"] call KyuFunctions_fnc_createTeleportBoard;
        sleep 1;
        [] execVM "scripts\missions\Mission_5.sqf";
        sleep 5;
        [ "AO_4_1_task" ] call BIS_fnc_deleteTask;
        [ "AO_4_2_task" ] call BIS_fnc_deleteTask;
        [ "AO_4_3_task" ] call BIS_fnc_deleteTask;
        sleep 600;
        deleteVehicle _teleport;
    };
};