player addEventHandler ["Take", {
    params ["_unit", "_container", "_item"];

    if(!([_unit, _item] call KyuFunctions_fnc_checkItemPermission)) then {
        systemChat "You can't take items you haven't unlocked yet.";
    };
}];