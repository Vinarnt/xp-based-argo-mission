#define layer_1_name "intro_1"
#define layer_2_name "intro_2"

_text = getText (missionConfigFile >> "introText") +
        "<br /><br />Developped by Kyu";

_camera = "camera" camCreate [6264.4, 4748.79, 1.12198];
_camera cameraEffect ["internal", "back"];
_camera setDir 95;
_camera camCommit 0;
showCinemaBorder true;

uiNamespace setVariable ["introCamera", _camera];

_layer1 = layer_1_name call BIS_fnc_rscLayer;
_layer2 = layer_2_name call BIS_fnc_rscLayer;
[_text, 0, 0.2, 9999999, 1, 0, _layer1] spawn BIS_fnc_dynamicText;

uiSleep 2;
_handlerID = findDisplay 46 displayAddEventHandler ["KeyDown", {
    params ["_display", "_key"];

    if(_key == 0x39) then {
        _display displayRemoveEventHandler ["KeyDown", uiNamespace getVariable "introSpaceHandler"];

        (layer_1_name call BIS_fnc_rscLayer) cutText ["", "PLAIN"];
        (layer_2_name call BIS_fnc_rscLayer) cutText ["", "PLAIN"];
        forceRespawn player;

        _camera = uiNamespace getVariable "introCamera";
        _camera cameraEffect ["terminate","back"];
        camDestroy _camera;
        showCinemaBorder false;

        uiNamespace setVariable ["introCamera", nil];
        uiNamespace setVariable ["introSpaceHandler", nil];
    };
}];
uiNamespace setVariable ["introSpaceHandler", _handlerID];

["Press <t color='#FFBF00'>SPACE</t> to continue", 0, 0.7, 9999999, 1, 0, _layer2] spawn BIS_fnc_dynamicText;