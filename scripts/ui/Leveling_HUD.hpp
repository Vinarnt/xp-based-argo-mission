#define POS_X (safezoneX + safezoneW - (safezoneW * 0.16))
#define POS_Y (safezoneY + 0.80 * safezoneH)
#define WIDTH (safezoneW * 0.15)
#define HEIGHT (safezoneW * 0.05)

class Leveling_HUD {
	idd = -1;
	fadein = 0;
	fadeout = 0;
	duration = 99999999;
	onLoad = "uiNamespace setVariable ['Leveling_HUD', _this select 0]";
	
	class ControlsBackground {
        class Background : RscBackgroundGUI {
            idc = -1;
            x = POS_X;
            y = POS_Y;
            w = WIDTH;
            h = HEIGHT - 0.015;
        };
	};
	class Controls {
        class RscStructuredText_1510: ctrlARGOStructuredText {
            idc = 1510;
            x = POS_X;
            y = POS_Y;
            w = WIDTH;
            h = HEIGHT;
            font = "TahomaB";
        };
        class RscProgress_1511: ctrlARGOProgress {
            idc = 1511;
            x = POS_X + 0.005 * safezoneW;
            y = POS_Y + 0.025 * safezoneH;
            w = WIDTH - 0.01 * safezoneW;
            h = 0.030;
        };
		class RscStructuredText_1512: ctrlARGOStructuredText {
			idc = 1512;
            x = POS_X + 0.005 * safezoneW;
            y = POS_Y + 0.025 * safezoneH;
            w = WIDTH;
            h = 0.030;
            font = "RobotoCondensed";
		};
	};
};