class Crew_HUD
{
	idd = -1;
	fadein = 0;
	fadeout = 0;
	duration = 99999999;
	onLoad = "uiNamespace setVariable ['Crew_HUD', _this select 0]";
	
	class ControlsBackground
	{

	};
	class Controls
	{
		class RscStructuredText_1501: RscStructuredText
		{
			idc = 1501;
            x = safezoneX + 0.01 * safezoneW;
        	y = safezoneY + 0.3 * safezoneH;
        	w = safezoneW * 0.2;
        	h = safezoneH * 0.4;
		};
	};
};