class SpecialWeapons_Dialog
{
	idd = -1;
	
	class ControlsBackground
	{
		class Background_2200: RscBackgroundGUIDark
		{
			idc = 2200;
			x = 20.5 * GUI_GRID_W + GUI_GRID_X;
			y = 10 * GUI_GRID_H + GUI_GRID_Y;
			w = 24.5 * GUI_GRID_W;
			h = 12.5 * GUI_GRID_H;
		};
		
	};
	class Controls
	{
		class RscListbox_1500: RscListbox
		{
			idc = 1500;
			x = 21.5 * GUI_GRID_W + GUI_GRID_X;
			y = 11 * GUI_GRID_H + GUI_GRID_Y;
			w = 22.5 * GUI_GRID_W;
			h = 9.5 * GUI_GRID_H;
			onLoad = "_this call KyuFunctions_fnc_populateSpecialWeaponsListBox";
		};
		class RscButtonMenuClose_2700: RscButtonMenuCancel
		{
			x = 38 * GUI_GRID_W + GUI_GRID_X;
			y = 21 * GUI_GRID_H + GUI_GRID_Y;
			w = 6 * GUI_GRID_W;
			h = 1 * GUI_GRID_H;
			text = "Close";
		};
		class RscButtonMenuOK_2600: RscButtonMenu
		{
			x = 31 * GUI_GRID_W + GUI_GRID_X;
			y = 21 * GUI_GRID_H + GUI_GRID_Y;
			w = 6 * GUI_GRID_W;
			h = 1 * GUI_GRID_H;
			text = "Ok";
			action = "call KyuFunctions_fnc_handleSpecialWeaponsOkButton";
		};
	};
};