#include "default_actions.sqf"

dev_actions = [
    //_x params ["_name","_code", "_args","_who", "_what", "_condition", "_texture_on", "_texture_off"];
];

rad_step = 0;
start_vector = [0, 0, 0];
end_vector = [0, 0, 0];
radial = 0;
radial_idx = 0;
rad_Nmin = 8;

eval_actions = {
    _available = [];
    _i = 0;
    {
        _x params ["_name","_code", "_args", "_who", "_what", "_condition", "_texture_on", "_texture_off"];
        _enabled = true;
        if (!([_name, _who,_what, _args] call _condition)) then {
               _enabled = false;
        };
        if (_enabled) then {
            _available append [_i];
        };
        _i= _i + 1;
    } forEach dev_actions;

    // return
    _available;
};

onEachFrame {
    _available = [] call grab_update;
    
    if ((count _available) > 0) then {
        _step = 360 / (rad_Nmin max (count _available));
        _i = 0;
        _r = 0;
        {
            _enabled = _i in _available;
            
            if (_enabled) then {
                _x params ["_name","_code", "_args", "_who", "_what", "_condition", "_texture_on", "_texture_off"];
                _offset = [[0, 0.5], _step * _r] call BIS_fnc_rotateVector2D;
                _offset = positionCameraToWorld [_offset select 0, _offset select 1, 2];
                _textSize = 0.05;
                _texture = 0;
                if (radial_idx == _i) then {
                    _textSize = 0.05;
                    _texture = _texture_on;
                } else {
                    _textSize = 0.03;
                    _texture = _texture_off;
                };
                drawIcon3D [_texture, [1, 1, 1, 1], _offset , 1, 1, 0, _name, 2, _textSize, "PuristaMedium"];
                //drawLine3D [positionCameraToWorld [0,0,2], _offset, [1,1,1,1]];
                _r = _r + 1;
            };
            _i = _i + 1;
        } forEach dev_actions;
    };
};

radial_selection={
    if (count _this > 0) then {
        _step= 360 / (rad_Nmin max (count _this));
        _i = 0;
        _r = 0;
        radial_idx = -1;
        _idx_val = 999;
        {
           _enabled = _i in _this;
           if (_enabled) then {
              _s = 360 - _step * _r;
              _c = selectMin [abs (radial - _s), abs (radial + 360 - _s) , abs (radial - 360 - _s)];
               
              if (_c < _idx_val) then {
                 radial_idx =_i;
                 _idx_val = _c;
              };
              _r = _r + 1;
           };
           _i = _i + 1;
        } forEach dev_actions;
    } else {
        radial_idx = -1;
    };    
};

grab_start = {
    if (rad_step == 0) then
    {
        {
            _who = player;
            _what = _x;
            _distance = (_what getVariable ["radial_mindist", 10.0]);
            _raw_actions = (_what getVariable ["radialcmd", []]);
            {
              _x params [
                  ["_name","default"],
                  ["_code",{}],
                  ["_args",[]],
                  ["_condition", {
                    params ["_action", "_who", "_what"];

                    (_who distance _what) < (_what getVariable ["radial_mindist", 10.0]);
                  }],
                  ["_texture_on",
                  getText (configFile >> "CfgVehicles" >> typeOf (_what) >> "picture")],
                  ["_texture_off",""]
              ];
              dev_actions append [[_name, _code, _args, _who, _what, _condition, _texture_on, _texture_off]];
            } forEach _raw_actions + (_what call get_default_action);

        } forEach [player, cursorObject];
        
        if (count dev_actions > 0) then {
            rad_step = 1;
            copyToClipboard str(dev_actions);
            start_vector = (getCameraViewDirection  player);
        };
    };
};


grab_hold = {
    if (rad_step == 1) then {
        end_vector = (getCameraViewDirection  player);
        
        mat_s = [start_vector, [0, 0, 1]] call MOP_fnc_vectorDirAndUp2DCM;
        mat_e = [end_vector, [0, 0, 1]] call MOP_fnc_vectorDirAndUp2DCM;
        _m = vectorMagnitude (start_vector vectorDiff end_vector);
        
        mat_f = [0, 90, 0] call MOP_fnc_RPY2DCM;
        
        ([mat_s] call MOP_fnc_DCM2RPY) params ["_s_r", "_s_p", "_s_y"];
        ([mat_e] call MOP_fnc_DCM2RPY) params ["_e_r", "_e_p", "_e_y"];
        
        nmat = [_e_r - _s_r,_e_p - _s_p, _e_y - _s_y] call MOP_fnc_RPY2DCM;
        nmat = [mat_f, nmat] call MOP_fnc_matrixMultiply;
        
        ([nmat] call MOP_fnc_DCM2RPY) params ["_f_r", "_f_p", "_f_y"];
        _f_r = _f_r * -1 + 180;
       
        dump setDir (_f_y);
        [dump, _f_p, 0] call BIS_fnc_setPitchBank;
        
        _offset = [[0, _m], 360 - _f_r] call BIS_fnc_rotateVector2D;
        _offset = positionCameraToWorld [_offset select 0, _offset select 1, 3];
        drawLine3D [positionCameraToWorld [0, 0, 2], _offset, [1, 0.2, 0.2, 1]];
        drawIcon3D ["", [1, 1, 1, 1], positionCameraToWorld [0, -0.05, 2] , 1, 1, 45, str((".." + str(_f_r) + " " + str(_m))), 1, 0.03, "PuristaMedium"];
        radial = _f_r;
        
        if (_m >= 0.08) then {
            _this call radial_selection;
        } else {
            radial_idx = -1;
        };
    };
};

grab_call = {
    (dev_actions select radial_idx) params ["_name", "_code", "_args", "_who", "_what", "_condition", "_texture_on", "_texture_off"];
    [_name, _who, _what, _args] call _code;
};

grab_release = {
    if (rad_step == 1) then {
        if (radial_idx > -1) then {
            [] call grab_call;
        };
        rad_step = 0;
        radial_idx = -1;
        dev_actions = [];
    };
};

grab_update = {
    _available = [] call eval_actions;
    if (rad_step == 0 && (inputAction "lookAround") > 0 ) then {
        [] call grab_start;
    } else {
        if (rad_step == 1 && (inputAction "lookAround") > 0) then {
            _available call grab_hold;
        } else {
            [] call grab_release;
        };
    };

    // return
    _available;
};