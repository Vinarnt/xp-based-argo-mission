/*
	author: Kyu
	description: Display who is in the vehicle
	returns: nothing
*/

// TODO: Use event handlers instead of redrawing infinitely

if(!hasInterface) exitWith {};

[] spawn {
    disableSerialization;

    _show = false;
    _layer = ["crewHud"] call BIS_fnc_rscLayer;

    while {true} do  {
        _lastShow = _show;
        _show = player != vehicle player;
        if(_show) then {
            _text = "<t color='#FDEE24' underline='1'>Vehicle crew</t><br/>";
            _vehicle = assignedVehicle player;
            _crew = crew _vehicle;
            _names = _crew apply {
                if(alive _x) then {
                    if(driver _vehicle == _x || {gunner _vehicle == _x}) then {
                        if(driver _vehicle == _x) then {
                            format ["<img size='0.8' image='a3\ui_f\data\IGUI\Cfg\Actions\getindriver_ca.paa'/> %1<br/>", name _x];
                        } else {
                            format ["<img size='0.8' image='a3\ui_f\data\IGUI\Cfg\Actions\getingunner_ca.paa'/> %1<br/>", name _x];
                        };
                    } else {
                        format ["<img size='0.8' image='a3\ui_f\data\IGUI\Cfg\Actions\getincargo_ca.paa'/> %1<br/>", name _x];
                    };
                } else {
                    "";
                };
            };
            _text = _text + (_names joinString "");

            if(!(_lastShow isEqualTo _show && {_show})) then {
                _layer cutRsc ["Crew_HUD", "PLAIN", -1, true];
            };

            _display = uiNamespace getVariable "Crew_HUD";
            _textCtrl = _display displayCtrl 1501;
            _textCtrl ctrlSetStructuredText parseText _text;
        } else {
            if(!(_lastShow isEqualTo _show && {!_show})) then {
                _layer cutText ["", "PLAIN"];
            };
        };
        uiSleep 0.5;
    };
};