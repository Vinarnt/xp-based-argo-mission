["no_dlc", "onEachFrame", {
        disableSerialization;
        _display=(findDisplay 46);
        {
            if ((toLower(ctrlText _x) find "lshift+p") >= 0 ||
                (toLower(ctrlText _x) find "vehicle locked") >=0 ||
                (toLower(ctrlText _x) find "_logo_ca.paa") >=0 ||
                ([-1,11406,1010,102,100,103,101] find (ctrlIDC _x))>=0) then {
                    _x ctrlSetPosition [-1,-1,-1,-1];
                    _x ctrlShow false;
                    _x ctrlCommit 0;
            };
        } forEach (allControls _display);
}]call BIS_fnc_addStackedEventHandler;