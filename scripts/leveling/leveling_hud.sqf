/*
	author: Kyu
	description: Display leveling info
	returns: nothing
*/
#include "define.hpp"

if (!hasInterface) exitWith {};

[] spawn {
    disableSerialization;

    waitUntil {
        sleep 0.25;
        not isNil "isLevelingInit" && {isLevelingInit};
    };

    _show = false;
    _layer = ["levelingHud"] call BIS_fnc_rscLayer;
    _layer cutRsc ["Leveling_HUD", "PLAIN", -1, false];

    while {true} do  {
        _display = uiNamespace getVariable "Leveling_HUD";
        _levelTextCtrl = _display displayCtrl 1510;
        _progressCtrl = _display displayCtrl 1511;
        _progressTextCtrl = _display displayCtrl 1512;

        _totalXp = call fnc_getXP;
        _level = _totalXp call fnc_getLevel;
        _lowerTotalNeededXp = LEVEL_DATA select (_level - 1);
        _totalNeededXp = LEVEL_DATA select _level;
        _xp = (round (_totalXp - _lowerTotalNeededXp));
        _neededXp = _totalNeededXp - _lowerTotalNeededXp;
        _completionProgress = _xp / _neededXp;

        _levelTextCtrl ctrlSetStructuredText parseText format ["<t size='0.8'>Level %1</t>", _level];
        _progressTextCtrl ctrlSetStructuredText parseText format ["<t align='center' size='0.6' color='#000000'>%1 / %2</t>", _xp, _neededXp];
        _progressCtrl progressSetPosition _completionProgress;

        uiSleep 0.25;
    };
};

diag_log "leveling hud initialized";