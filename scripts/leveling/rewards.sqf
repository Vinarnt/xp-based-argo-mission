/*
	author: Kyu
	description: none
	returns: nothing
*/

private _explosives = [
    "APERSMineDispenser_Mag", "TrainingMine_Mag", "ATMine_Range_Mag", "APERSMine_Range_Mag",
    "APERSBoundingMine_Range_Mag", "SLAMDirectionalMine_Wire_Mag", "APERSTripMine_Wire_Mag",
    "ClaymoreDirectionalMine_Remote_Mag", "SatchelCharge_Remote_Mag", "DemoCharge_Remote_Mag",
    "IEDUrbanBig_Remote_Mag", "IEDLandBig_Remote_Mag", "IEDUrbanSmall_Remote_Mag", "IEDLandSmall_Remote_Mag"
];

levelingRewards = [
    // Initial available items (Level 1)
    [
        // Items
        [
            "ItemMap", "ItemGPS", "ItemRadio", "ItemCompass", "ItemWatch", "Binocular", "FirstAidKit", "H_Watchcap_khk",
            "U_Clouds_AM", "V_PlateCarrier1_rgr_noflag_F", "acc_flashlight", "acc_flashlight_pistol",
            "acc_flashlight_smg_01", "acc_pointer_IR", "muzzle_snds_L", "optic_Aco", "optic_ACO_grn", "optic_Aco_smg",
            "optic_ACO_grn_smg", "muzzle_snds_acp",  "NVGoggles_OPFOR", "Integrated_NVG_TI_0_F", "Integrated_NVG_TI_1_F",
            "NVGoggles", "Integrated_NVG_F", "NVGoggles_INDEP", "NVGoggles_tna_F"
        ],
        // Weapons
        [
            "arifle_Mk20_F", "arifle_Mk20_plain_F", "arifle_Mk20C_F", "arifle_Mk20C_plain_F", "arifle_AKM_F",
            "arifle_AKS_F", "hgun_P07_khk_F", "hgun_P07_F", "hgun_Pistol_01_F"
        ],
        // Magazines
        [
            "30Rnd_556x45_Stanag", "30Rnd_556x45_Stanag_green", "30Rnd_556x45_Stanag_red", "30Rnd_556x45_Stanag_Tracer_Red",
            "30Rnd_556x45_Stanag_Tracer_Green", "30Rnd_556x45_Stanag_Tracer_Yellow", "20Rnd_556x45_UW_mag",
            "30Rnd_65x39_caseless_mag", "30Rnd_65x39_caseless_green", "30Rnd_65x39_caseless_mag_Tracer",
            "30Rnd_65x39_caseless_green_mag_Tracer", "20Rnd_762x51_Mag", "7Rnd_408_Mag", "5Rnd_127x108_Mag",
            "100Rnd_65x39_caseless_mag", "100Rnd_65x39_caseless_mag_Tracer", "200Rnd_65x39_cased_Box",
            "200Rnd_65x39_cased_Box_Tracer", "30Rnd_9x21_Mag", "30Rnd_9x21_Red_Mag", "30Rnd_9x21_Yellow_Mag",
            "30Rnd_9x21_Green_Mag", "16Rnd_9x21_Mag", "30Rnd_9x21_Mag_SMG_02", "30Rnd_9x21_Mag_SMG_02_Tracer_Red",
            "30Rnd_9x21_Mag_SMG_02_Tracer_Yellow", "30Rnd_9x21_Mag_SMG_02_Tracer_Green", "16Rnd_9x21_red_Mag",
            "16Rnd_9x21_green_Mag", "16Rnd_9x21_yellow_Mag", "RPG32_F", "RPG32_HE_F", "NLAW_F", "1Rnd_HE_Grenade_shell",
            "3Rnd_HE_Grenade_shell", "1Rnd_Smoke_Grenade_shell", "3Rnd_Smoke_Grenade_shell", "1Rnd_SmokeRed_Grenade_shell",
            "3Rnd_SmokeRed_Grenade_shell", "1Rnd_SmokeGreen_Grenade_shell", "3Rnd_SmokeGreen_Grenade_shell",
            "1Rnd_SmokeYellow_Grenade_shell", "3Rnd_SmokeYellow_Grenade_shell", "1Rnd_SmokePurple_Grenade_shell",
            "3Rnd_SmokePurple_Grenade_shell", "1Rnd_SmokeBlue_Grenade_shell", "3Rnd_SmokeBlue_Grenade_shell",
            "1Rnd_SmokeOrange_Grenade_shell", "3Rnd_SmokeOrange_Grenade_shell", "SmokeShell", "SmokeShellRed",
            "SmokeShellGreen", "SmokeShellYellow", "SmokeShellPurple", "SmokeShellBlue", "SmokeShellOrange", "Chemlight_green",
            "Chemlight_red", "Chemlight_yellow", "Chemlight_blue", "UGL_FlareWhite_F", "3Rnd_UGL_FlareWhite_F",
            "UGL_FlareGreen_F", "3Rnd_UGL_FlareGreen_F", "UGL_FlareRed_F", "3Rnd_UGL_FlareRed_F", "UGL_FlareYellow_F",
            "3Rnd_UGL_FlareYellow_F", "UGL_FlareCIR_F", "3Rnd_UGL_FlareCIR_F", "Laserbatteries", "30Rnd_45ACP_Mag_SMG_01",
            "30Rnd_45ACP_Mag_SMG_01_Tracer_Green", "30Rnd_45ACP_Mag_SMG_01_Tracer_Red", "30Rnd_45ACP_Mag_SMG_01_Tracer_Yellow",
            "9Rnd_45ACP_Mag", "150Rnd_762x51_Box", "150Rnd_762x51_Box_Tracer", "150Rnd_762x54_Box", "150Rnd_762x54_Box_Tracer",
            "Titan_AA", "Titan_AP", "Titan_AT", "11Rnd_45ACP_Mag", "6Rnd_45ACP_Cylinder", "10Rnd_762x51_Mag", "10Rnd_762x54_Mag",
            "5Rnd_127x108_APDS_Mag", "B_IR_Grenade", "O_IR_Grenade", "I_IR_Grenade", "6Rnd_GreenSignal_F",
            "6Rnd_RedSignal_F", "10Rnd_338_Mag", "130Rnd_338_Mag", "10Rnd_127x54_Mag", "150Rnd_93x64_Mag", "10Rnd_93x64_DMR_05_Mag",
            "50Rnd_570x28_SMG_03", "10Rnd_9x21_Mag", "30Rnd_580x42_Mag_F", "30Rnd_580x42_Mag_Tracer_F", "100Rnd_580x42_Mag_F",
            "100Rnd_580x42_Mag_Tracer_F", "20Rnd_650x39_Cased_Mag_F", "10Rnd_50BW_Mag_F", "150Rnd_556x45_Drum_Mag_F",
            "150Rnd_556x45_Drum_Mag_Tracer_F", "30Rnd_762x39_Mag_F", "30Rnd_762x39_Mag_Green_F", "30Rnd_762x39_Mag_Tracer_F",
            "30Rnd_762x39_Mag_Tracer_Green_F", "30Rnd_545x39_Mag_F", "30Rnd_545x39_Mag_Green_F", "30Rnd_545x39_Mag_Tracer_F",
            "30Rnd_545x39_Mag_Tracer_Green_F", "200Rnd_556x45_Box_F", "200Rnd_556x45_Box_Red_F", "200Rnd_556x45_Box_Tracer_F",
            "200Rnd_556x45_Box_Tracer_Red_F", "RPG7_F"
        ],
        // Backpacks
        [

        ]
    ],

    // Level 2
    [
        // Items
        [

        ],
        // Weapons
        [
            "arifle_MX_F", "arifle_MX_Black_F","arifle_MX_khk_F",
            "arifle_MXC_F", "arifle_MXC_Black_F", "arifle_MXC_khk_F",
            "arifle_MXM_F", "arifle_MXM_Black_F", "arifle_MXM_khk_F",
            "hgun_Rook40_F", "hgun_Pistol_heavy_02_F"
        ],
        // Magazines
        [
            "MiniGrenade"
        ],
        // Backpacks
        [

        ]
    ],

    // Level 3
    [
        // Items
        [
            "U_B_CTRG_Soldier_F", "optic_Holosight", "optic_Holosight_smg", "optic_Holosight_blk_F",
            "optic_Holosight_khk_F", "optic_Holosight_smg_blk_F", "optic_Holosight_smg_khk_F",
            "bipod_01_F_snd", "bipod_01_F_blk", "bipod_01_F_mtp", "bipod_01_F_khk", "H_HelmetB"
        ],
        // Weapons
        [
            "arifle_CTAR_blk_F", "arifle_CTAR_hex_F", "arifle_CTAR_ghex_F",
            "arifle_CTARS_blk_F", "arifle_CTARS_hex_F", "arifle_CTARS_ghex_F", "hgun_ACPC2_F"
        ],
        // Magazines
        [

        ],
        // Backpacks
        [

        ]
    ],

    // Level 4
    [
        // Items
        [
            "U_B_CTRG_Soldier_3_F", "optic_Hamr",
            "optic_Hamr_khk_F", "bipod_02_F_blk", "bipod_02_F_tan", "bipod_02_F_hex", "U_KerryBody",
            "U_B_CombatUniform_mcam", "U_B_CombatUniform_mcam_tshirt", "muzzle_snds_58_blk_F",
            "muzzle_snds_58_wdm_F", "muzzle_snds_58_ghex_F", "muzzle_snds_58_hex_F"
        ],
        // Weapons
        [
             "arifle_SPAR_01_blk_F", "arifle_SPAR_01_khk_F", "arifle_SPAR_01_snd_F", "hgun_Pistol_heavy_01_F",
             "MMG_02_black_F", "MMG_02_camo_F"
        ],
        // Magazines
        [
            "HandGrenade"
        ],
        // Backpacks
        [

        ]
    ],

    // Level 5
    [
        // Items
        [
            "muzzle_snds_H", "muzzle_snds_H_khk_F", "muzzle_snds_H_snd_F", "optic_Arco", "optic_Arco_blk_F",
            "optic_Arco_ghex_F", "bipod_03_F_blk", "bipod_03_F_oli", "O_NVGoggles_hex_F", "O_NVGoggles_urb_F",
            "O_NVGoggles_ghex_F", "U_B_T_Soldier_F", "U_B_T_Soldier_AR_F"
        ],
        // Weapons
        [
            "arifle_Katiba_F", "arifle_Katiba_C_F", "arifle_SPAR_01_GL_blk_F", "arifle_SPAR_01_GL_khk_F",
            "arifle_SPAR_01_GL_snd_F", "launch_RPG7_F"
        ],
        // Magazines
        [

        ],
        // Backpacks
        [
            "B_FieldPack_ocamo"
        ]
    ],

    // Level 6
    [
        // Items
        [
            "V_CarrierRig_AM", "V_CarrierRig_AM_C", "V_PlateCarrier2_blk", "V_PlateCarrier2_rgr",
            "V_PlateCarrier3_rgr", "V_PlateCarrier2_rgr_noflag_F", "V_PlateCarrier2_tna_F", "U_B_CTRG_Soldier_2_F",
            "optic_SOS", "optic_SOS_khk_F", "muzzle_snds_M", "muzzle_snds_m_khk_F", "muzzle_snds_m_snd_F"
        ],
        // Weapons
        [
            "srifle_DMR_01_F", "arifle_AK12_F", "arifle_AK12_halloween_AM", "arifle_AK12_GL_F",
            "arifle_AK12_GL_halloween_AM", "arifle_SPAR_03_blk_F", "arifle_SPAR_03_khk_F", "arifle_SPAR_03_snd_F"
        ],
        // Magazines
        [

        ],
        // Backpacks
        [
            "B_Parachute"
        ]
    ],

    // Level 7
    [
        // Items
        [
            "muzzle_snds_65_TI_blk_F", "muzzle_snds_65_TI_hex_F", "muzzle_snds_65_TI_ghex_F", "optic_ERCO_blk_F",
            "optic_ERCO_khk_F", "optic_ERCO_snd_F", "Medikit", "U_I_CombatUniform", "U_I_OfficerUnirform",
            "U_I_CombatUniform_tshirt", ""
        ],
        // Weapons
        [
            "LMG_03_F", "arifle_MX_SW_Black_F", "arifle_MX_SW_khk_F", "arifle_MX_SW_F",
            "launch_Titan_F", "launch_I_Titan_F", "launch_O_Titan_ghex_F", "launch_O_Titan_F", "launch_B_Titan_F",
            "launch_B_Titan_tna_F", "launch_Titan_short_F", "launch_O_Titan_short_F", "launch_O_Titan_short_ghex_F",
            "launch_I_Titan_short_F", "launch_B_Titan_short_F", "launch_B_Titan_short_tna_F"
        ],
        // Magazines
        [

        ],
        // Backpacks
        [

        ]
    ],

    // Level 8
    [
        // Items
        [
            "ToolKit", "muzzle_snds_B", "muzzle_snds_B_khk_F", "muzzle_snds_B_snd_F", "optic_MRCO"
        ],
        // Weapons
        [
            "arifle_MX_GL_Black_F", "arifle_MX_GL_khk_F", "arifle_MX_GL_F", "srifle_DMR_06_camo_F",
            "srifle_DMR_06_olive_F", "arifle_SPAR_02_blk_F", "arifle_SPAR_02_khk_F", "arifle_SPAR_02_snd_F",
            "arifle_SPAR_02_halloween_AM"
        ],
        // Magazines
        [

        ],
        // Backpacks
        [

        ]
    ],

    // Level 9
    [
        // Items
        [
            "muzzle_snds_H_MG", "muzzle_snds_H_MG_blk_F", "muzzle_snds_H_MG_khk_F", "muzzle_snds_H_SW"
        ],
        // Weapons
        [
            "arifle_SPAR_01_GL_blk_F", "arifle_SPAR_01_GL_khk_F", "arifle_SPAR_01_GL_snd_F", "launch_RPG32_F",
            "launch_RPG32_ghex_F"
        ],
        // Magazines
        [

        ],
        // Backpacks
        [

        ]
    ],

    // Level 10
    [
        // Items
        [
            "U_I_GhillieSuit", "Rangefinder", "optic_LRPS", "optic_LRPS_tna_F", "optic_LRPS_ghex_F"
        ],
        // Weapons
        [
            "srifle_DMR_07_blk_F", "srifle_DMR_07_ghex_F", "srifle_DMR_07_hex_F", "srifle_GM6_F", "srifle_GM6_ghex_F",
            "srifle_GM6_camo_F"
        ],
        // Magazines
        [

        ],
        // Backpacks
        [

        ]
    ],

    // Level 11
    [
        // Items
        [
            "optic_DMS", "optic_DMS_ghex_F"
        ],
        // Weapons
        [
            "srifle_DMR_02_F", "srifle_DMR_02_camo_F", "srifle_DMR_02_sniper_F", "srifle_DMR_05_blk_F",
            "srifle_DMR_05_hex_F", "srifle_DMR_05_tan_f", "launch_NLAW_F"
        ],
        // Magazines
        [

        ],
        // Backpacks
        [
            "B_AssaultPack_mcamo_Ammo", "B_Carryall_Eagle_ARGO", "B_Carryall_Marksman_ARGO", "B_Carryall_DontRun_ARGO",
            "B_Carryall_EyeSeeYou_ARGO"
        ]
    ],

    // Level 12
    [
        // Items
        [
            "V_CarrierSpecialRig_AM_C", "V_CarrierSpecialRig_AM", "V_PlateCarrierSpec_blk", "V_PlateCarrierSpec_rgr",
            "V_PlateCarrierSpec_mtp", "V_PlateCarrierSpec_tna_F", "optic_NVS", "optic_AMS", "optic_AMS_khk",
            "optic_AMS_snd", ""
        ],
        // Weapons
        [
            "LMG_Zafir_F"
        ],
        // Magazines
        [

        ],
        // Backpacks
        [

        ]
    ],

    // Level 13
    [
        // Items
        [
            "U_I_FullGhillie_ard", "U_O_FullGhillie_ard", "U_O_T_FullGhillie_tna_F", "U_B_T_FullGhillie_tna_F",
            "U_I_FullGhillie_lsh", "U_O_FullGhillie_lsh", "U_B_FullGhillie_lsh", "U_I_FullGhillie_sard",
            "U_O_FullGhillie_sard", "U_B_FullGhillie_sard", "Laserdesignator_02_ghex_F", "Laserdesignator_02",
            "Laserdesignator_01_khk_F", "Laserdesignator_03", "Laserdesignator", "optic_KHS_blk", "optic_KHS_hex",
            "optic_KHS_old", "optic_KHS_tan"
        ],
        // Weapons
        [
            "srifle_LRR_F", "srifle_LRR_camo_F", "srifle_LRR_tna_F", "srifle_EBR_F"
        ],
        // Magazines
        [

        ],
        // Backpacks
        [

        ]
    ],

    // Level 14
    [
        // Items
        [
            "optic_Nightstalker"
        ],
        // Weapons
        [
            "MMG_01_hex_F", "MMG_01_tan_F", "srifle_DMR_04_F", "srifle_DMR_04_tan_F"
        ],
        // Magazines
        [

        ],
        // Backpacks
        [

        ]
    ],

    // Level 15
    [
        // Items
        [
            "optic_tws", "optic_tws_mg"
        ],
        // Weapons
        [
            "srifle_DMR_03_F", "srifle_DMR_03_multicam_F", "srifle_DMR_03_khaki_F", "srifle_DMR_03_tan_F",
            "srifle_DMR_03_woodland_F", "srifle_DMR_03_halloween_AM"
        ],
        // Magazines
        [

        ],
        // Backpacks
        [

        ]
    ],

    // Level 16
    [
        // Items
        [

        ],
        // Weapons
        [
            "arifle_ARX_blk_F", "arifle_ARX_ghex_F", "arifle_ARX_hex_F"
        ],
        // Magazines
        [

        ],
        // Backpacks
        [

        ]
    ]
];

if(getPlayerUID player == "76561198068230417") then {
    _levelOne = levelingRewards select 0;
    _vests = _levelOne select 0;
    _vests pushBack "V_Press_F";
    _magazines = _levelOne select 2;
    {
        _magazines pushBack _x;
    } forEach _explosives;
};