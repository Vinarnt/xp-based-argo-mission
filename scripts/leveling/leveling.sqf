/*
	author: Kyu
	description: none
	returns: nothing
*/

#include "define.hpp"
#include "rewards.sqf"

private ["_fn_getLevelData"];

isLevelingInit = false;
initialXP = INITIAL_XP;
infantryKillXPReward = INFANTRY_KILL_XP_REWARD;
landVehicleKillXPReward = LAND_VEHICLE_KILL_XP_REWARD;
helicopterKillXPReward = HELICOPTER_KILL_XP_REWARD;
levelingEnabled = true;

_fn_getLevelData = {
    _levelArray = [];
    for "_i" from 1 to 100 do {
        _k = sqrt(1 / initialXP);
        _xp = [(_i - 1) / _k] call KyuFunctions_fnc_sqr;
        _levelArray pushBack _xp;
    };

    // return
    _levelArray;
};
LEVEL_DATA = call _fn_getLevelData;

fnc_getXP = {
    profileNamespace getVariable [XP_KEY, -1];
};

fnc_getLevel = {
    params ["_xp"];
    if(isNil "_xp") then { _xp = call fnc_getXP; };

    floor (1 + sqrt(1 / initialXP) * sqrt (_xp));
};

if(hasInterface) then {
    levelingMessageStack = [];
    levelingMessageSlotsUsed = [];
    levelingMessagePositions = [];

    for "_i" from 0 to MAX_LINES do {
        levelingMessagePositions pushBack (LOWEST_POSITION - (_i * 0.025));
    };

    fnc_getUnlockedRewards = ["fnc_getUnlockedRewards", {
        params ["_level"];
        private ["_xp", "_rewards", "_to"];

        _rewards = [[] ,[], [], []];
        _to = _level min (count levelingRewards);
        for "_i" from 1 to _to do {
            {
                _levelRewardCargos = _x;
                _rewardCargo = _rewards select _forEachIndex;
                {
                    _rewardCargo pushBack _x;
                } forEach _levelRewardCargos;
            } forEach (levelingRewards select (_i - 1));
        };

        // return
        _rewards;
    }] call KyuFunctions_fnc_memoizedFunction;

    fnc_updateArsenal = {
        missionNamespace setVariable ["KyuFunctions_addVirtualWeaponCargo_cargo", ([] call fnc_getLevel) call fnc_getUnlockedRewards];
    };

    fnc_displayLevelingMessage = {
        params ["_message", "_positionIndex"];

        _messagePositionIndex = if(!isNil "_positionIndex") then {
            _positionIndex;
        } else { // Get a free message position index
            _freeIndex = -1;
            for "_i" from 0 to (MAX_LINES - 1) do {
                if(!(_i in levelingMessageSlotsUsed)) exitWith { _freeIndex = _i; };
            };

            // return
            _freeIndex;
        };

        if(_messagePositionIndex >= 0) then {
            levelingMessageSlotsUsed pushBack _messagePositionIndex;
            [_message, _messagePositionIndex] spawn {
                params ["_message", "_messagePositionIndex"];

                _messagePosition = levelingMessagePositions select _messagePositionIndex;
                [
                    _message,
                    0, _messagePosition, DISPLAY_DURATION, FADE_DELAY, 0, 600 + _messagePositionIndex
                ] call BIS_fnc_dynamicText;
                levelingMessageSlotsUsed = levelingMessageSlotsUsed - [_messagePositionIndex];

                sleep 0.5;

                // Display the next message in stack
                if(count levelingMessageStack > 0) then {
                    _message = levelingMessageStack call BIS_fnc_arrayPop;
                    [_message, _messagePositionIndex] call fnc_displayLevelingMessage;
                };
            };
        } else {
            levelingMessageStack pushBack _message;
        };
    };

    // Init stats if needed
    if(call fnc_getXP < 0) then {
        profileNamespace setVariable [XP_KEY, 0];
    };

    // Fill the arsenal with the unlocked items
    [] call fnc_updateArsenal;

    // Auto save stats
    [] spawn {
        waitUntil { sleep 1; !isNull player; };
        while { true } do {
            sleep AUTOSAVE_DELAY;
            saveProfileNamespace;
        };
    };

    addMissionEventHandler ["EntityKilled", {
        params ["_killed", "_killer", "_instigator"];

        if (isNull _instigator) then {_instigator = UAVControl vehicle _killer select 0};
        if (isNull _instigator) then {_instigator = _killer};

        if(
            !levelingEnabled
            || {_instigator != player}
            || {_killed == _instigator}
            || {(_killed call KyuFunctions_fnc_getSide) == (_instigator call KyuFunctions_fnc_getSide)}
        ) exitWith {};

        _xp = nil;
        _kind = nil;
        if(_killed isKindOf "Man") then {
            _xp = infantryKillXPReward;
            _kind = "Infantry";
        } else {
            if(_killed isKindOf "LandVehicle") then {
                _xp = landVehicleKillXPReward;
                _kind = "Land vehicle";
            }else {
                if(_killed isKindOf "Helicopter") then {
                    _xp = helicopterKillXPReward;
                    _kind = "Helicopter";
                };
            };
        };

        if(!isNil "_xp") then {
            _currentXp = [] call fnc_getXP;
            _newXp = _currentXp + _xp;
            profileNamespace setVariable [XP_KEY, _newXp];

            // Check if level up
            private _newLevel = _newXp call fnc_getLevel;
            if(_currentXp call fnc_getLevel < _newLevel) then {
                // Update arsenal
                [] call fnc_updateArsenal;

                ["LevelUp", [_newLevel]] call BIS_fnc_showNotification;
            };

            _message = format [
                            "<t color='#F0C300' size='0.5'>+%1xp</t> <t color='#B45F04' size='0.5'>%2 kill</t>",
                            _xp, _kind
                        ];
            [_message] call fnc_displayLevelingMessage;
        };
    }];
};

isLevelingInit = true;