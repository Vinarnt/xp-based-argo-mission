/*
	author: Kyu
	description: none
	returns: nothing
*/

params ["_name"];

private [
    "_crewList"
    ,"_crewInventoryList"
    ,"_crewSkillList"
	,"_crewVarNameList"
    ,"_infantryList"
    ,"_infantryInventoryList"
    ,"_infantrySkillList"
	,"_infantryVarNameList"
	,"_infantryPositionList"
    ,"_newGroup"
    ,"_newVehicle"
    ,"_unitGroup"
    ,"_groupSide"
    ,"_groupPos"
    ,"_groupDir"
    ,"_groupVars"
    ,"_special"
    ,"_vehicleHitpointsDamageList"
    ,"_vehicleHealthList"
    ,"_vehicleFuelList"
    ,"_vehicleList"
    ,"_vehiclePositionList"
    ,"_vehicleItemList"
    ,"_vehicleLockedList"
    ,"_vehicleMagazineList"
    ,"_vehiclePylonList"
    ,"_vehicleWeaponList"
    ,"_vehicleBackpackList"
    ,"_vehicleMaterialsList"
    ,"_vehicleTexturesList"
    ,"_vehicleAnimationNames"
    ,"_vehicleAnimationPhases"
	,"_vehicleVarNameList"
    ,"_waypointList"
];
    _groupData = missionNamespace getVariable ("savedGroup_" +_name);
    if(isNil "_groupData") exitWith { nil };

    _vehicleList = _groupData select 0;
    _vehicleHitpointsDamageList = _groupData select 1;
    _vehicleHealthList = _groupData select 2;
    _vehicleFuelList = _groupData select 3;
    _vehiclePositionList = _groupData select 4;
    _vehicleLockedList = _groupData select 5;
    _vehicleItemList = _groupData select 6;
    _vehicleMagazineList = _groupData select 7;
    _vehicleWeaponList = _groupData select 8;
    _vehicleBackpackList = _groupData select 9;
    _vehicleMaterialsList = _groupData select 10;
    _vehicleTexturesList = _groupData select 11;
    _vehiclePylonList = _groupData select 12;
    _vehicleAnimationNames = _groupData select 13;
    _vehicleAnimationPhases = _groupData select 14;
    _vehicleVarNameList = _groupData select 15;
    _crewList = _groupData select 16;
    _crewInventoryList = _groupData select 17;
    _crewSkillList = _groupData select 18;
    _crewVarNameList = _groupData select 19;
    _infantryList = _groupData select 20;
    _infantryInventoryList = _groupData select 21;
    _infantrySkillList = _groupData select 22;
    _infantryVarNameList = _groupData select 23;
    _infantryPositionList = _groupData select 24;
    _groupSide = _groupData select 25;
    _groupPos = _groupData select 26;
    _groupDir = _groupData select 27;
    _groupVars = _groupData select 28;
    _waypointList = _groupData select 29;
    _special = _groupData select 30;

    _newGroup = createGroup _groupSide;

    { _newGroup setVariable [_x select 0, _x select 1] } forEach _groupVars;

    for "_vehicleIndex" from 0 to (count _vehicleList - 1) do {
        private "_newVehiclePosition";

        _newVehicleType = (_vehicleList select _vehicleIndex);
        _newVehicle = createVehicle [_newVehicleType, _groupPos, [], 0, _special];
        _newGroup setFormDir _groupDir;
        _newVehicle setDir _groupDir;
        _newGroup addVehicle _newVehicle;
        _newVehicle enableSimulationGlobal false;
        _newVehicle lock (_vehicleLockedList select _vehicleIndex);
        _newVehicle setDamage (_vehicleHealthList select _vehicleIndex);
        _newVehicle setFuel (_vehicleFuelList select _vehicleIndex);
        _hitpoint = (_vehicleHitpointsDamageList select _vehicleIndex) select 0;
        _hitpointDamage = (_vehicleHitpointsDamageList select _vehicleIndex) select 2;
        {
            _newVehicle setHitPointDamage [_x, _hitpointDamage select _forEachIndex];
        } forEach _hitpoint;
        clearItemCargoGlobal _newVehicle;
        clearMagazineCargoGlobal _newVehicle;
        clearWeaponCargoGlobal _newVehicle;
        clearBackpackCargoGlobal _newVehicle;
        {
            _newVehicle addItemCargoGlobal [_x,1];
        } forEach (_vehicleItemList select _vehicleIndex);
        {
            _newVehicle addMagazineCargoGlobal [_x,1];
        } forEach (_vehicleMagazineList select _vehicleIndex);
        {
            _newVehicle addWeaponCargoGlobal [_x,1];
        } forEach (_vehicleWeaponList select _vehicleIndex);
        {
            _newVehicle addBackpackCargoGlobal [_x,1];
        } forEach (_vehicleBackpackList select _vehicleIndex);
        {
            _newVehicle setPylonLoadOut [(_forEachIndex + 1), _x];
        } forEach (_vehiclePylonList select _vehicleIndex);
        {
            _newVehicle setObjectMaterialGlobal [_forEachIndex, _x];
        } forEach (_vehicleMaterialsList select _vehicleIndex);
        {
            _newVehicle setObjectTextureGlobal [_forEachIndex, _x];
        } forEach (_vehicleTexturesList select _vehicleIndex);

        _thisAnimationNames = _vehicleAnimationNames select _vehicleIndex;
        _thisAnimationPhases = _vehicleAnimationPhases select _vehicleIndex;
        {
            _newVehicle animateSource [_x, _thisAnimationPhases select _forEachIndex];
        } forEach _thisAnimationNames;
		_newVehicleVarName = (_vehicleVarNameList select _vehicleIndex);
		if (!(_newVehicleVarName isEqualTo "")) then {
			[_newVehicle, _newVehicleVarName] remoteExec ["setVehicleVarName", 0, _newVehicle];
			missionNamespace setVariable [_newVehicleVarName, _newVehicle, true];
		};

        {
            private _crewUnit = _newGroup createUnit [_x, _groupPos, [], 0, "FORM"];
            _tmpInventory = _crewInventoryList select _vehicleIndex;
            _crewUnit setUnitLoadout (_tmpInventory select _forEachIndex);
            _tmpSkill = _crewSkillList select _vehicleIndex;
            _crewUnit setSkill (_tmpSkill select _forEachIndex);
			_tmpVarName = (_crewVarNameList select _vehicleIndex) select _forEachIndex;
			if (!(_tmpVarName isEqualTo "")) then {
				[_crewUnit, _tmpVarName] remoteExec ["setVehicleVarName", 0, _crewUnit];
				missionNamespace setVariable [_tmpVarName, _crewUnit, true];
			};
            _crewUnit moveInAny _newVehicle;
        } forEach (_crewList select _vehicleIndex);

        if (_newVehicle isKindOf "Plane" && _special == "FLY") then {
            _newVehicle setVelocity [
                60 * sin _groupDir,
                60 * cos _groupDir,
                0
            ];
        };

        _newVehicle enableSimulationGlobal true;

        {_x addCuratorEditableObjects [[_newVehicle],true]} forEach allCurators;
    };

    //Spawn infantry
    {
        if(isDev) then {
            "Sign_Arrow_Yellow_F" createVehicle (_infantryPositionList select _forEachIndex);
        };
        private _unit = _newGroup createUnit [
            (_infantryList select _forEachIndex),
            (_infantryPositionList select _forEachIndex),
            [], 0, "CAN_COLLIDE"
        ];
        _unit enableSimulation false;
        _unit setUnitLoadout (_infantryInventoryList select _forEachIndex);
        _unit setSkill (_infantrySkillList select _forEachIndex);
		_tmpVarName = (_infantryVarNameList select _forEachIndex);
		if (!(_tmpVarName isEqualTo "")) then {
			[_unit, _tmpVarName] remoteExec ["setVehicleVarName", 0, _unit];
			missionNamespace setVariable [_tmpVarName, _unit, true];
		};
    } forEach _infantryList;

    //Apply waypoints
    [_newGroup, _waypointList] call KyuFunctions_fnc_applyWaypoints;

    // return
    _newGroup