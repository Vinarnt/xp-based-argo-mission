/*
	author: Kyu
	description: none
	returns: nothing
*/

params ["_name", "_unitGroup", ["_delete", false], ["_special", "NONE"]];

private [
    "_crewList"
    ,"_crewInventoryList"
    ,"_crewSkillList"
	,"_crewVarNameList"
	,"_infantrySkipList"
    ,"_infantryList"
    ,"_infantryInventoryList"
    ,"_infantrySkillList"
	,"_infantryVarNameList"
	,"_infantryPositionList"
    ,"_unitGroup"
    ,"_groupSide"
    ,"_groupPos"
    ,"_unitsInGroup"
    ,"_groupVars"
    ,"_special"
    ,"_vehicleHitpointsDamageList"
    ,"_vehicleHealthList"
    ,"_vehicleFuelList"
    ,"_vehicleList"
    ,"_vehiclePositionList"
    ,"_vehicleItemList"
    ,"_vehicleLockedList"
    ,"_vehicleMagazineList"
    ,"_vehiclePylonList"
    ,"_vehicleWeaponList"
    ,"_vehicleBackpackList"
    ,"_vehicleMaterialsList"
    ,"_vehicleTexturesList"
    ,"_vehicleAnimationNames"
    ,"_vehicleAnimationPhases"
	,"_vehicleVarNameList"
    ,"_waypointList"
];

_leader = leader _unitGroup;
_unitsInGroup = units _unitGroup;
_groupSide = side _unitGroup;
_groupDir = getDir _leader;
_groupPos = getPos _leader;
_groupVars = [];

_waypointList = [];
_infantrySkipList = [];
_infantryList = [];
_infantryInventoryList = [];
_infantrySkillList = [];
_infantryVarNameList = [];
_infantryPositionList = [];
_vehicleList = [];
_vehicleHitpointsDamageList = [];
_vehicleHealthList = [];
_vehicleFuelList = [];
_vehiclePositionList = [];
_vehicleLockedList = [];
_vehicleItemList = [];
_vehicleMagazineList = [];
_vehiclePylonList = [];
_vehicleWeaponList = [];
_vehicleBackpackList = [];
_vehicleMaterialsList = [];
_vehicleTexturesList = [];
_vehicleAnimationNames = [];
_vehicleAnimationPhases = [];
_vehicleVarNameList = [];
_crewList = [];
_crewInventoryList = [];
_crewSkillList = [];
_crewVarNameList = [];

// Freeze units
{
    _x enableSimulationGlobal false;
} forEach _unitsInGroup;

// Save waypoint data
_waypointList = [_unitGroup] call KyuFunctions_fnc_saveWaypoints;

// Save group variables
{
    _varValue = _unitGroup getVariable _x;
    if(!(isNil "_varValue")) then {
        _groupVars pushBack [_x, _varValue];
    };
} forEach ["TCL_Hold", "TCL_Defend", "TCL_Custom", "TCL_Disabled", "TCL_Idle", "TCL_Freeze", "TCL_Default", "TCL_Enhanced"];

// Save unit data and delete
{
    _currentVehicle = vehicle _x;
    if (_currentVehicle isKindOf "LandVehicle" || _currentVehicle isKindOf "Air" || _currentVehicle isKindOf "Ship") then {
        _vehicleList append [typeOf _currentVehicle];
        _vehicleHitpointsDamageList append [getAllHitPointsDamage _currentVehicle];
        _vehicleHealthList append [damage _currentVehicle];
        _vehicleFuelList append [fuel _currentVehicle];
        _vehiclePositionList append [getPos _currentVehicle];
        _vehicleLockedList append [locked _currentVehicle];
        _vehicleItemList append [itemCargo _currentVehicle];
        _vehicleMagazineList append [magazineCargo _currentVehicle];
        _vehicleWeaponList append [weaponCargo _currentVehicle];
        _vehicleBackpackList append [backpackCargo _currentVehicle];
        _vehicleMaterialsList append [getObjectMaterials _currentVehicle];
        _vehicleTexturesList append [getObjectTextures _currentVehicle];
        _vehiclePylonList append [getPylonMagazines _currentVehicle];

        _thisAnimationNames = animationNames _currentVehicle;
        _thisAnimationPhases = [];
        {
            _thisAnimationPhases append [_currentVehicle animationPhase _x];
        } forEach _thisAnimationNames;
        _vehicleAnimationNames append [_thisAnimationNames];
        _vehicleAnimationPhases append [_thisAnimationPhases];
		_vehicleVarNameList append [vehicleVarName _currentVehicle];

        _tmpCrew = crew _currentVehicle;
        if(_delete) then {
            deleteVehicle _currentVehicle;
        };
        _tmpCrewList = [];
        _tmpCrewInventoryList = [];
        _tmpCrewSkillList = [];
		_tmpCrewVarNameList = [];
        {
            _tmpCrewList append [typeOf (vehicle _x)];
            _tmpCrewInventoryList append [getUnitLoadout _x];
            _tmpCrewSkillList append [skill _x];
			_tmpCrewVarNameList append [vehicleVarName _x];
			_infantrySkipList append [_x];
            if(_delete) then {
                deleteVehicle _x;
            };
        } forEach _tmpCrew;

        _crewList set [(count _vehicleList - 1), _tmpCrewList];
        _crewInventoryList set [(count _vehicleList - 1), _tmpCrewInventoryList];
        _crewSkillList set [(count _vehicleList - 1), _tmpCrewSkillList];
		_crewVarNameList set [(count _vehicleList - 1), _tmpCrewVarNameList];
    } else {
        if(!(_x in _infantrySkipList)) then {
             _infantryList append [typeOf (vehicle _x)];
             _infantryInventoryList append [getUnitLoadout _x];
             _infantrySkillList append [skill _x];
             _infantryVarNameList append [vehicleVarName _x];
             _infantryPositionList append [position _x];
             if(_delete) then {
                deleteVehicle (vehicle _x);
             };
        };
    };
} forEach _unitsInGroup;

if(!_delete) then {
    // Unfreeze units
    {
        _x enableSimulationGlobal true;
    } forEach _unitsInGroup;
} else {
    deleteGroup _unitGroup;
};

missionNamespace setVariable ["savedGroup_" + _name,
        [
            _vehicleList, _vehicleHitpointsDamageList, _vehicleHealthList, _vehicleFuelList, _vehiclePositionList,
            _vehicleLockedList, _vehicleItemList, _vehicleMagazineList, _vehicleWeaponList, _vehicleBackpackList,
            _vehicleMaterialsList, _vehicleTexturesList, _vehiclePylonList, _vehicleAnimationNames, _vehicleAnimationPhases,
            _vehicleVarNameList, _crewList, _crewInventoryList, _crewSkillList, _crewVarNameList, _infantryList,
            _infantryInventoryList, _infantrySkillList, _infantryVarNameList, _infantryPositionList, _groupSide,
            _groupPos, _groupDir, _groupVars, _waypointList, _special
        ]
];