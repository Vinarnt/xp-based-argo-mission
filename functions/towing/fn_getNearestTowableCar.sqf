/*
	author: Kyu
	description: none
	returns: nothing
*/

params ["_fromCar", ["_radius", 50]];

_nearestCars = nearestObjects [_fromCar, ["Car"], _radius];
_nearestCars = [_nearestCars, [_fromCar], { _input0 distance _x }, "DESCEND"] call BIS_fnc_sortBy;
_nearestCar =  [_nearestCars, {
    params ["_o"];

    (_o != _fromCar) && !([_o] call KyuFunctions_fnc_isTowed) && !([_o] call KyuFunctions_fnc_isTowing);
}] call KyuFunctions_fnc_arrayFindIf;

// return
_nearestCar;