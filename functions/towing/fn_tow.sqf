/*
	author: Kyu
	description: none
	returns: nothing
*/
params ["_from", "_to"];

_boundingBox = boundingBox _from;
_minCoords = _boundingBox select 0;
_maxCoords = _boundingBox select 1;

_fromLength = abs ((_maxCoords select 1) - (_minCoords select 1));

_boundingBox = boundingBox _to;
_minCoords = _boundingBox select 0;
_maxCoords = _boundingBox select 1;

_toLength = abs ((_maxCoords select 1) - (_minCoords select 1));

_from attachTo [_to, [0, -_toLength, 0]];
_rope = ropeCreate [_to, [0, -_toLength * 0.35, -0.3], _from, [0, _fromLength * 0.1, -0.2]];

_resetHandler = {
    params ["_unit"];

    // Remove towing state before respawn
    _unit setVariable ["isTowing", false, true];
    _unit setVariable ["isTowed", false, true];
    _unit setVariable ["towedTo", nil, true];
};
_from addEventHandler ["killed", _resetHandler];
_to addEventHandler ["killed", _resetHandler];

_from setVariable ["isTowed", true, true];
_to setVariable ["isTowing", true, true];
_from setVariable ["towedTo", _to, true];
_to setVariable ["towedTo", _from, true];