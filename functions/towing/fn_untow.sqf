/*
	author: Kyu
	description: none
	returns: nothing
*/
params ["_towedCar"];

_towingCar = _towedCar getVariable "towedTo";

detach _towedCar;
_ropes = ropes _towingCar;

// destroy all ropes, there can be more than one if the tow/untow system is spammed
{
    _rope = _x;
    _towedCar ropeDetach _rope;

    [_rope] spawn {
        params ["_rope"];

        ropeUnwind [_rope, 2, 0];
        waitUntil {
            sleep 0.1;
            ropeUnwound _rope;
        };
        ropeDestroy _rope;
    };
} forEach _ropes;

_towedCar setVariable ["isTowed", false, true];
_towingCar setVariable ["isTowing", false, true];
_towedCar setVariable ["towedTo", nil, true];
_towingCar setVariable ["towedTo", nil, true];

// Update position to the ground
_towedCar setPos position _towedCar;