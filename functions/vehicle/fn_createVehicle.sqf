params ["_class", "_position", "_azimuth", ["_side", west], ["_texture", ""], ["_shouldRespawn", true]];

_vehicle = createVehicle [_class, _position, [], 0, "NONE"];
_vehicle setPos _position;
_vehicle setDir _azimuth;
_vehicle setVariable ["side", _side, true];

if (_texture != "") then { _vehicle setObjectTextureGlobal [0, _texture]; };
if(_shouldRespawn) then {
	_vehicle setVariable ["texture", _texture];
	_vehicle call KyuFunctions_fnc_autoRespawnVehicle;
};

// return
_vehicle