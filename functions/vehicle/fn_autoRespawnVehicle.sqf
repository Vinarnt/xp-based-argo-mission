#define DESERTED_RESPAWN_DELAY 120
#define DESTROYED_RESPAWN_DELAY 60

params ["_vehicle"];

if ( ! ( isServer ) ) exitWith {};

[_vehicle, DESERTED_RESPAWN_DELAY, DESTROYED_RESPAWN_DELAY] spawn KyuFunctions_fnc_vehicleRespawn;