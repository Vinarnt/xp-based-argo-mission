/*
	author: Kyu
	description: Get the classes of al special weapons
	returns: Array
*/

private ["_weapons"];

_weapons = [
    "launch_NLAW_F",
    "launch_RPG32_F",
    "launch_Titan_F",
    "launch_Titan_short_F",
    "launch_RPG7_F",
    "srifle_GM6_F",
    "srifle_LRR_F"
];

// return
_weapons;