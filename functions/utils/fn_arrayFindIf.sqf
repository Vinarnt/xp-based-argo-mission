/*
	author: Kyu
	description: Get the first item in the array validating the predicate
	returns: The item found or nil if not
*/

params ["_array", "_predicate"];

_value = nil;
{ // forEach

    if([_x] call _predicate) then {
        _value = _x;
        breakOut "";
    };
} forEach _array;

// return
_value;