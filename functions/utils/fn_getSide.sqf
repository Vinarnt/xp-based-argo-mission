/*
	author: Kyu
	description: Get the side of a unit, vehicle, group or location
	returns: The side
*/

private _target = param [0, objNull, [objNull, grpNull, locationNull]];

// If it's a vehicle
if(_target isKindOf "AllVehicles") then {
    private ["_side"];

    // Check the side variable
    _side = _target getVariable "side";
    if(isNil "_side") then {
        // Check crew side
        if({ _x != _target} count (crew _target) > 0) then {
            _side = side ((crew _target) select 0);
        } else {
            // Check faction of the vehicle
            _side = switch (faction _target) do {
                case "BLU_Clouds_AM": { west };
                case "OPF_Flames_AM": { east };
                default { side _target };
            };
        };
    };

    //return
    _side;
} else {
    // return
    side _target;
};