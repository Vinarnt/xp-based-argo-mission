/*
	author: Kyu
	description: Memoized a function.
	             Meaning it will call the function only once until the arguments passed to the
	             memoized function changes.
	             Useful for huge computational functions that may be called several times with the same arguments.
	arguments: functionName - The name of the memoized function
	           functionCode - The code executed upon recompute
	returns: The memoized function
*/

private _functionName = param [0, nil, [""]];
private _functionCode = param [1, nil, [{}]];

if(isNil "_functionName") exitWith { ["_functionName parameter missing"] call BIS_fnc_error };
if(isNil "_functionCode") exitWith { ["_functionCode parameter missing"] call BIS_fnc_error };

_memoizedFunctionName = format ["%1_memoized", _functionName];
_memoizedFunction = missionNamespace getVariable _memoizedFunctionName;
if(isNil "_memoizedFunction") then {
    missionNamespace setVariable [_memoizedFunctionName, [[], []]];

    _memoizedFunction = compileFinal ("
        private _functionCode = " + (str _functionCode) + ";

        _memoizedData = missionNamespace getVariable '" + (_memoizedFunctionName) + "';
        _returnValue = if(!([_this, (_memoizedData select 0)] call BIS_fnc_areEqual)) then {
            _functionCode = _functionCode;
            _returnValue = _this call _functionCode;
            _memoizedFunction = [_this, _returnValue];
            missionNamespace setVariable ['" + (_memoizedFunctionName) + "', _memoizedFunction];

            _returnValue;
        } else {
            _memoizedData select 1;
        };

        _returnValue;
    ");
};

// return
_memoizedFunction;