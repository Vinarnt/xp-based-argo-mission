private _unit = param [0, objNull, [objNull]];
private _item = param [1, nil, [""]];

_rewards = (([] call fnc_getLevel) call fnc_getUnlockedRewards);
_has = false;
{
    if(_item in _x) exitWith { _has = true; };
} forEach _rewards;

private _checkSuccess = true;
private _weaponConfig = configFile >> "CfgWeapons";
if(!_has) then {
    if(
        _item isKindOf ["RifleCore", _weaponConfig]
        || {_item isKindOf ["PistolCore", _weaponConfig]}
        || {_item isKindOf ["LauncherCore", _weaponConfig]}
    ) exitWith {
        _unit removeWeapon _item;
        _checkSuccess = false;
    };

    if(_item isKindOf ["ItemCore", _weaponConfig]) exitWith {
        _unit removePrimaryWeaponItem _item;
        _unit removeSecondaryWeaponItem _item;
        _unit removeHandgunItem _item;
        _unit unlinkItem _item;
        _unit removeItem _item;
        _checkSuccess = false;
    };

    if(_item isKindOf ["Binocular", _weaponConfig]) exitWith {
        _unit unlinkItem _item;
        removeGoggles _unit;
        _unit removeWeapon _item;
        _checkSuccess = false;
    };

    if(_item isKindOf ["Uniform_Base", _weaponConfig]) exitWith {
        removeUniform _unit;
        _checkSuccess = false;
    };

    if(
        _item isKindOf ["Vest_Camo_Base", _weaponConfig]
        || _item isKindOf ["Vest_NoCamo_Base", _weaponConfig]
    ) exitWith {
        removeVest _unit;
        _checkSuccess = false;
    };

    if(_item isKindOf ["Bag_Base", configFile >> "CfgVehicles"]) exitWith {
        removeBackpack _unit;
        _checkSuccess = false;
    };
} else {
    // Check containers

    private _itemRewards = (_rewards select 0) + (_rewards select 2);

    // Uniforms
    if(_item isKindOf ["Uniform_Base", _weaponConfig]) exitWith {
        {
            if(!(_x in _itemRewards)) then {
                _unit removeItemFromUniform _x;
                _checkSuccess = false;
            };
        } forEach uniformItems _unit;
    };

    // Vests
    if(
        _item isKindOf ["Vest_Camo_Base", _weaponConfig]
        || _item isKindOf ["Vest_NoCamo_Base", _weaponConfig]
    ) exitWith {
        {
            if(!(_x in _itemRewards)) then {
                _unit removeItemFromVest _x;
                _checkSuccess = false;
            };
        } forEach vestItems _unit;
    };

    // Backpacks
    if(_item isKindOf ["Bag_Base", configFile >> "CfgVehicles"]) exitWith {
        {
            if(!(_x in _itemRewards)) then {
                _unit removeItemFromBackpack _x;
                _checkSuccess = false;
            };
        } forEach backpackItems _unit;
    }
};

// return
_checkSuccess;