params ["_position"];

// declare a few variables we need and make _sign randomly negative
private _sign = 1;
if (floor random 10 > 4) then { _sign = -1 };
private _flareArray = ["WHITE", "RED", "GREEN", "YELLOW"];

// organize our arguments
private _duration = 30;
private _flareDist = 100;
private _delay = 0.2;
private _delayRandom = 0.2;
private _flareHeight = 200;
private _flareHeightRandom = 25;
private _flareSpeed = -8;

private _endTime = time + _duration;

// create loop for spawning flares
while { _endTime > time } do {
	_flareType = _flareArray call BIS_fnc_selectRandom;

	// assign colors
	switch (_flareType) do {
		case "WHITE": 	{ _flareType = "F_40mm_White" };
		case "RED": 	{ _flareType = "F_40mm_Red" };
		case "GREEN": 	{ _flareType = "F_40mm_Green" };
		case "YELLOW": 	{ _flareType = "F_40mm_Yellow" };
	};

	// get a random spot around the target
	private _pos = [_position, random _flareDist, random 360] call BIS_fnc_relPos;
	_pos = [_pos select 0, _pos select 1, _flareHeight + (random _flareHeightRandom * _sign)];
	// make the flare at that spot
	private _flare = _flareType createVehicle _pos;
	// set its speed
	_flare setVelocity [0, 0, _flareSpeed];
	// delay plus random delay
	sleep _delay + random _delayRandom;
};