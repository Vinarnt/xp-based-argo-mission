/*
	author: Kyu
	description: Create a board with an action to teleport to the defined location
	args: marker - Marker where the board should be created
	      to - Position to teleport to (Can be "base" or a position array)
	returns: the created vehicle or nothing if failed
*/

params ["_marker", ["_to", "base"]];

private ["_position", "_direction", "_toPosition"];

_position = markerPos _marker;
_direction = markerDir _marker;

switch (true) do {
    case (typeName _to == "STRING" &&  {_to isEqualTo "base"}): {
        _toPosition = TELEPORT_BASE_POSITION;
    };
    case (typeName _to == "ARRAY"): {
        _toPosition = _to;
    };
    default {
        diag_log format ["Unsupported type %1 for '_to' parameter", typeName _to];
    };
};

if(!(isNil "_toPosition")) then {
    _vehicle = "MapBoard_Malden_F" createVehicle _position;
    _vehicle setPos _position;
    _vehicle setDir _direction;
    _vehicle allowDamage false;
    _vehicle enableSimulation false;

    [_vehicle, ["Go back to base", {player setPos (_this select 3)}, _toPosition]] remoteExec ["addAction"];

    // return
    _vehicle;
};