/*
	author: Kyu
	description: none
	returns: nothing
*/

params ["_heli"];

_heli setSlingLoad objNull;
_cargo = _targetHeli getVariable "slingLoadCargo";
_cargo setVariable ["isSlingLoaded", false, true];
_cargo setVariable ["slingLoadedBy", nil, true];
_heli setVariable ["isSlingLoading", false, true];
_heli setVariable ["slingLoadCargo", nil, true];