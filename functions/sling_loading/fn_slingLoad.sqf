/*
	author: Kyu
	description: none
	returns: nothing
*/

params ["_cargo", "_heli"];

_success = _heli setSlingLoad _cargo;
if(_success) then {
    _cargo setVariable ["isSlingLoaded", true, true];
    _cargo setVariable ["slingLoadedBy", _heli, true];
    _heli setVariable ["isSlingLoading", true, true];
    _heli setVariable ["slingLoadCargo", _cargo, true];

    _cargo addEventHandler ["killed", {
        params ["_cargo"];

         _cargo setVariable ["isSlingLoaded", false, true];
         _cargo setVariable ["slingLoadedBy", nil, true];
    }];
    _heli addEventHandler ["killed", {
        params ["_heli"];

         _heli setVariable ["isSlingLoading", false, true];
         _heli setVariable ["slingLoadCargo", nil, true];
    }];
};

// return
_success;