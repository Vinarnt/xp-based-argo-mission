/*
	author: Kyu
	description: none
	returns: nothing
*/

params ["_marker"];

_marker setMarkerText "Heli Repair";
_trigger = [_marker, "Helicopter"] call KyuFunctions_fnc_createRepairArea;