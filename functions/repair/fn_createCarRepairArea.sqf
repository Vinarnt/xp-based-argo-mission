/*
	author: Kyu
	description: none
	returns: nothing
*/

params ["_marker"];

_marker setMarkerText "Car Repair";
_trigger = [_marker, "Car"] call KyuFunctions_fnc_createRepairArea;