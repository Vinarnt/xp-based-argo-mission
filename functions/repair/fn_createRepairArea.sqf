/*
	author: Kyu
	description: none
	returns: nothing
*/

params ["_marker", "_vehicleClass"];

private ["_trigger"];

_markerPos = markerPos _marker;
_varName = format ["trigger_%1_%2", round (_markerPos select 0), round (_markerPos select 1)];

if(isServer) then {
    _trigger = createTrigger ["EmptyDetector", markerPos _marker];
    call (compile format ["%1 = _trigger;", _varName]);
    publicVariable _varName;
};

if(!isDedicated) then {
    [_varName, _marker, _vehicleClass] spawn {
        params ["_varName", "_marker", "_vehicleClass"];

        _trigger = nil;
        waitUntil {!isNil _varName};
        call (compile format ["_trigger = %1;", _varName]);
        _trigger setTriggerArea [20, 20, markerDir _marker, true];
        _trigger setTriggerActivation ["ANYPLAYER", "PRESENT", true];
        _trigger setTriggerStatements [
            format [
                "_vehicle = vehicle player;
                this && player != _vehicle && _vehicle isKindOf ""%1""
                && _vehicle in thisList && isTouchingGround _vehicle
                && round (speed _vehicle) == 0;",
                _vehicleClass
            ],

            "[vehicle player] spawn {
                 params [""_vehicle""];

                 _vehicle setDamage 0;
                 _vehicle setFuel 1;
                 _vehicle setVehicleAmmo 1;
                 systemChat ""Repaired"";
             };",

             ""
        ];
    };
};
