params ["_slider"];

_rain = rain;

_slider sliderSetRange [0, 100];
_slider sliderSetPosition (_rain * 100);
_slider ctrlSetTooltip format['Rain %1', _rain];
_slider ctrlCommit 0;
_slider ctrlAddEventHandler ["SliderPosChanged", {
    params ["_control", "_newValue"];

    _rain = _newValue / 100;
    _control ctrlSetTooltip format['Rain %1', _rain];
    _control ctrlCommit 0;
}];
uiNamespace setVariable['admin_rain_slider', _slider];