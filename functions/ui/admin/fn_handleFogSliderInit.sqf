params ["_slider"];

_fog = fog;

_slider sliderSetRange [0, 100];
_slider sliderSetPosition (_fog * 100);
_slider ctrlSetTooltip format['Fog %1', _fog];
_slider ctrlCommit 0;
_slider ctrlAddEventHandler ["SliderPosChanged", {
    params ["_control", "_newValue"];

    _fog = _newValue / 100;
    _control ctrlSetTooltip format['Fog %1', _fog];
    _control ctrlCommit 0;
}];
uiNamespace setVariable['admin_fog_slider', _slider];