/*
	author: Kyu
	description: none
	returns: nothing
*/

private ["_index", "_className"];

_index = lbCurSel 1500;
_className = lbData [1500, _index];
player addWeaponGlobal _className;
