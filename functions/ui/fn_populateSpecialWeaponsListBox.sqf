/*
	author: Kyu
	description: none
	returns: nothing
*/

params ["_listBox"];

private ["_weapons"];

_weapons = call KyuFunctions_fnc_getSpecialWeapons;

{
    _name = getText(configFile >> "CfgWeapons" >> _x >> "displayName");
    _picture = getText(configFile >> "CfgWeapons" >> _x >> "picture");
    _index = _listBox lbAdd _name;
    _listBox lbSetPicture [_index, _picture];
    _listBox lbSetData [_index, _x];
} forEach _weapons;

