params ["_hour"];

systemChat format ["Set time to %1", [_hour, "HH:MM:SS"] call BIS_fnc_timeToString];


[(_hour - daytime + 24 ) % 24] remoteExec ["skipTime", 0];