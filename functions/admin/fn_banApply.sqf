params ['_type','_admin'];

switch (_type) do 
{
	case ('INIT'):
	{
		disableSerialization;
		private['_display'];
		waituntil {_display = findDisplay 46; !isnull _display};//???
		_display displayAddEventHandler ["KeyDown","call AdminFunctions_fnc_banAllChat"];
	};
	
	case ('AC'): //Anti hack
	{
		[] spawn
		{
			_speedC=0;
			_recoilC=0;
			_randTime= time+ random (15);
			while {true} do
			{
				//anti cheat////////////////////////////////////////////////////////////
				_res= false;
				if (alive player) then
				{
					
					
					//recoil
					_myRecoil = unitRecoilCoefficient player;
					_coef =  getCustomAimCoef player;
					if (_coef<0.5 or _myRecoil<1) then
					{
						if (alive player) then
						{
							_recoilC= _recoilC+1;
							if (_recoilC>15) then
							{
								_recoilC=0;
								[format['>>> %1: i''m recoil hacker A: %2 R: %3', name player, _coef, _myRecoil]]remoteExec ["systemchat", 0, false];
								_res= true;
							};
						};
					};
					///////////////
					
					//speed hack// man only
					_vp= vehicle player;
					_max= getNumber (configfile >> "CfgVehicles" >> typeOf _vp >> "maxSpeed")+1;
					if (speed _vp > _max and _vp isKindOf 'MAN' and isTouchingGround _vp) then //_max!=0 and isTouchingGround _vp
					{
						if (alive player) then
						{
							_speedC=_speedC+1;
							if (_speedC>10) then
							{
								_speedC=0;
								[format['>>> %1: i''m speed hacker speed: %2', name player, speed _vp]] remoteExec ["systemchat", 0, false];
								player setUnconscious true;
								_res= true;
							};
						};
					};
					///////////////
					
					if (_res) then //catched
					{
						//player setDamage 1;
					};
					
					//uniform
					if (time>_randTime) then 
					{
						if (uniform player == "U_O_T_Soldier_Hidden_F") then 
						{
							//player spawn MAD_fnc_lightning; 
							[player, format['HACKER (%1)', name player]] remoteExec ['sideChat', _admin, false];
							[player, 'RAIDEN: I caught one little bastard. You cannot hide from retaliation.'] remoteExec ['systemChat', -2, false];
							_randTime= time+ 5 +random (15);
						};
					};
					 
					
				} else {_speedC=0; _recoilC=0;};
				sleep 0.1;
			};	
		};
	};
	
	case ('VTBAN'): //Voice and Txt ban
	{
		_status= !(missionNamespace getvariable['s3A_VTBAN', false]);
		missionNamespace setvariable['s3A_VTBAN', _status];
		if (_status) then { HINT 'You voice chat blocked' } else  { HINT 'You voice chat unblocked' };
	};
	
	case ('PARAMSTEST'):
	{
		if !(alive player) exitWith {'Info: ',[format['Not alive yet: %1', name player]] remoteExec ['HintC', _admin, false]};
		
		_myRecoil= unitRecoilCoefficient player;
		_coef=  getCustomAimCoef player;
		_spd= getAnimSpeedCoef player;
		_txt= [];
		
		//Experimental
		_od= damage player;
		_d= 0.01+ random 0.1;
		player setDamage (_d+_od);
		sleep 0.15;
		if !(damage player isEqualTo (_d+_od)) then
		{
			if (alive player) then
			{
				//[format['>>> %1: i''m heal hacker', name player]]remoteExec ["systemchat", BAN_E getVariable['_admin', objNull], false];	
				_txt pushBack format['>>> %1: i''m heal hacker', name player];
			};
		} else 
		{
			player setDamage (_od);
		};
		
		if (alive player and isObjectHidden player) then
		{
			_txt pushBack format['>>> %1: i''m invisible hacker', name player];	
		};
		
		_txt pushBack format['UID: %1', getPlayerUID player];
		_txt pushBack format['Recoil Coefficient [%1]', _myRecoil];
		_txt pushBack format['Custom Aim Coefficient [%1]', _coef];
		_txt pushBack format['Vehicle: %1 side: %2 Damage: %3 (%4) Type: %5 Uniform: %6', player, side player, isDamageAllowed player, damage player, typeOf vehicle player, uniform player]; 
		_txt pushBack format['SpeedA: %1 Speed: %2', _spd, speed vehicle player]; 
			
		[format['Params test: %1', name player], _txt] remoteExec ['HintC', _admin, false];
	};	

};//SW