params ["_group"];

_vehicles = [];
{
    _vehicle = vehicle _x;
    if(_vehicle == _x) then { deleteVehicle _x }
    else {
        _vehicles pushBackUnique _vehicle;
        _vehicle deleteVehicleCrew _x;
    };
} forEach units _group;

{ deleteVehicle _x } forEach _vehicles;