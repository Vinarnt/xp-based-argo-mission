params ["_leader"];

null = [_leader] spawn {
    params ["_leader"];

    _cargo = assignedCargo (vehicle (_leader));
    {
        unassignVehicle _x;
        moveOut _x;
        sleep 0.4;
        _x addBackpack "B_Parachute";
    } forEach _cargo;
};