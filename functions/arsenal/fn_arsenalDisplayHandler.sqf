_mode = _this select 0;
_params = _this select 1;

switch _mode do {
	case "onLoad": {
		if (isNil {missionNamespace getVariable "KyuFunctions_fnc_arsenal_data"}) then {
			startLoadingScreen [""];
			['Init', _params] spawn KyuFunctions_fnc_arsenal;
		} else {
			['Init', _params] call KyuFunctions_fnc_arsenal;
		};
	};
	case "onUnload": {
		['Exit', _params] call KyuFunctions_fnc_arsenal;
	};
};