/*
	Author: Karel Moricky

	Description:
	Get virtual weapons to an object (e.g., ammo box).
	Virtual items can be selected in the Arsenal.

	Parameter(s):
		0: OBJECT

	Returns:
	ARRAY of ARRAYs - all virtual items within the object's space in format [<items>,<weapons>,<magazines>,<backpacks>]
*/

private ["_object"];
_object = _this param [0,missionNamespace,[missionNamespace,objNull]];
[_object,[],false,false,0,1] call KyuFunctions_fnc_addVirtualItemCargo;