private ["_center","_path","_custom","_delete","_namespace","_name"];
_center = _this param [0,player,[objNull]];
_path = _this param [1,[],[[]]];
_custom = _this param [2,[],[[]]];
_delete = _this param [3,false,[false]];

_namespace = _path param [0,missionNamespace,[missionNamespace,grpNull,objNull]];
_name = _path param [1,"",[""]];

//--- Get magazines loaded to weapons
private ["_primaryWeaponMagazine","_secondaryWeaponMagazine","_handgunMagazine"];
_primaryWeaponMagazine = "";
_secondaryWeaponMagazine = "";
_handgunMagazine = "";
{
	if (count _x > 4 && {typeName (_x select 4) == typeName []}) then {
		private ["_weapon","_magazine"];
		_weapon = _x select 0;
		_magazine = _x select 4 select 0;
		if !(isNil "_magazine") then {
			switch _weapon do {
				case (primaryWeapon _center): {_primaryWeaponMagazine = _magazine;};
				case (secondaryWeapon _center): {_secondaryWeaponMagazine = _magazine;};
				case (handgunWeapon _center): {_handgunMagazine = _magazine;};
			};
		};
	};
} forEach weaponsItems _center;

//--- Get current values
private ["_export"];
_export = [
	/* 00 */	[uniform _center,uniformItems _center],
	/* 01 */	[vest _center,vestItems _center],
	/* 02 */	[backpack _center,backpackItems _center],
	/* 03 */	headgear _center,
	/* 04 */	goggles _center,
	/* 05 */	binocular _center,
	/* 06 */	[primaryWeapon _center call bis_fnc_baseWeapon,_center weaponAccessories primaryWeapon _center,_primaryWeaponMagazine],
	/* 07 */	[secondaryWeapon _center call bis_fnc_baseWeapon,_center weaponAccessories secondaryWeapon _center,_secondaryWeaponMagazine],
	/* 08 */	[handgunWeapon _center call bis_fnc_baseWeapon,_center weaponAccessories handgunWeapon _center,_handgunMagazine],
	/* 09 */	assignedItems _center - [binocular _center],
	/* 10 */	_custom
];

//--- Store
private ["_data","_nameID"];
_data = _namespace getVariable ["KyuFunctions_fnc_saveInventory_data",[]];
_nameID = _data find _name;
if (_delete) then {
	if (_nameID >= 0) then {
		_data set [_nameID,objNull];
		_data set [_nameID + 1,objNull];
		_data = _data - [objNull];
	};
} else {
	if (_nameID < 0) then {
		_nameID = count _data;
		_data set [_nameID,_name];
	};
	_data set [_nameID + 1,_export];
};
_namespace setVariable ["KyuFunctions_fnc_saveInventory_data",_data];
profileNamespace setVariable ["KyuFunctions_fnc_saveInventory_profile",true];
if !(isNil {profileNamespace getVariable "KyuFunctions_fnc_saveInventory_profile"}) then {saveProfileNamespace};

_export