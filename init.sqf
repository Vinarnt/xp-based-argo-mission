isDev = false;

[] execVM "scripts\vehicle.sqf";
[] execVM "scripts\arsenal.sqf";
[] execVM "scripts\leveling\leveling.sqf";

BIS_syncedTime = 0;
BIS_matchStartT = 0;
setViewDistance 1100;
setObjectViewDistance 1300;
setTimeMultiplier 10;

TELEPORT_BASE_POSITION = markerPos "respawn_west_2";

if(isServer) then {
    TCL_Path = "TCL_System\";
    call compile preprocessFileLineNumbers (TCL_Path + "TCL_Preprocess.sqf");

    if(isDev) then {
        TCL_Debug = [false, false, false, false, false, true, true];
        TCL_Initialize = true;
    };

    [] spawn {
        _handle_1 = ["AO_1_group", AO_1_group, true] spawn KyuFunctions_fnc_saveGroup;
        _handle_2 = ["AO_2_group", AO_2_group, true] spawn KyuFunctions_fnc_saveGroup;
        _handle_3 = ["AO_2_group_1", AO_2_group_1, true] spawn KyuFunctions_fnc_saveGroup;
        _handle_4 = ["AO_3_group", AO_3_group, true] spawn KyuFunctions_fnc_saveGroup;
        _handle_5 = ["AO_4_group_1", AO_4_group_1, true] spawn KyuFunctions_fnc_saveGroup;
        _handle_6 = ["AO_4_group_2", AO_4_group_2, true] spawn KyuFunctions_fnc_saveGroup;
        _handle_7 = ["AO_4_group_3", AO_4_group_3, true] spawn KyuFunctions_fnc_saveGroup;
        _handle_8 = ["AO_5_group", AO_5_group, true] spawn KyuFunctions_fnc_saveGroup;
        _handle_9 = ["AO_6_group", AO_6_group, true] spawn KyuFunctions_fnc_saveGroup;
        _handle_10 = ["AO_6_group_1", AO_6_group_1, true, "FLY"] spawn KyuFunctions_fnc_saveGroup;
        _handle_11 = ["AO_6_group_2", AO_6_group_2, true] spawn KyuFunctions_fnc_saveGroup;
        _handle_12 = ["AO_7_group", AO_7_group, true] spawn KyuFunctions_fnc_saveGroup;
        _handle_13 = ["AO_8_group", AO_8_group, true] spawn KyuFunctions_fnc_saveGroup;
        _handle_14 = ["AO_9_group", AO_9_group, true] spawn KyuFunctions_fnc_saveGroup;
        _handle_15 = ["AO_10_group_1", AO_10_group_1, true] spawn KyuFunctions_fnc_saveGroup;
        _handle_16 = ["AO_10_group_2", AO_10_group_2, true] spawn KyuFunctions_fnc_saveGroup;
        _handle_17 = ["AO_10_group_3", AO_10_group_3, true] spawn KyuFunctions_fnc_saveGroup;

        {
            _x setMarkerAlpha 0;
        } forEach ["AO_1", "AO_2", "AO_3", "AO_5", "AO_6", "AO_8", "AO_9", "AO_10"];

        waitUntil {
            sleep 1;
            scriptDone _handle_1
            && {scriptDone _handle_2}
            && {scriptDone _handle_3}
            && {scriptDone _handle_4}
            && {scriptDone _handle_5}
            && {scriptDone _handle_6}
            && {scriptDone _handle_7}
            && {scriptDone _handle_8}
            && {scriptDone _handle_9}
            && {scriptDone _handle_10}
            && {scriptDone _handle_11}
            && {scriptDone _handle_12}
            && {scriptDone _handle_13}
            && {scriptDone _handle_14}
            && {scriptDone _handle_15}
            && {scriptDone _handle_16}
            && {scriptDone _handle_17};
        };
        if(!isDev) then {
            [] execVM "scripts\missions\Mission_1.sqf";
        } else{
            //[] execVM "scripts\missions\Mission_10.sqf";
        };
    };
};

// Init repair points
["v_repair_1"] spawn KyuFunctions_fnc_createCarRepairArea;
["v_repair_2"] spawn KyuFunctions_fnc_createHeliRepairArea;

diag_log "Initialized init";