if (missionNamespace getVariable ["firstRespawn", true]) then {
    missionNamespace setVariable ["firstRespawn", false];
    [player, [missionNamespace, "BIS_inv"]] call KyuFunctions_fnc_saveInventory;
} else {
    [player, [missionNamespace, "BIS_inv"]] call KyuFunctions_fnc_loadInventory;
};